package de.wiejack.kreator.kopier.api

@Retention(value = AnnotationRetention.SOURCE)
@Target(allowedTargets = [AnnotationTarget.CONSTRUCTOR])
annotation class CopyConstructor()
