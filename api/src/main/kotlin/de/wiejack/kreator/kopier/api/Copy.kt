package de.wiejack.kreator.kopier.api

@Retention(value = AnnotationRetention.SOURCE)
@Target(allowedTargets = [AnnotationTarget.CLASS])
annotation class Copy {
}
