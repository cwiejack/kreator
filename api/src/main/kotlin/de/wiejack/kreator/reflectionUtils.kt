package de.wiejack.kreator

import java.lang.reflect.Field

fun findField(clazz: Class<*>, name: String?): Field? {
    var searchType: Class<*>? = clazz
    while (Any::class.java != searchType && searchType != null) {
        val fields: Array<Field> = getDeclaredFields(searchType)
        for (field in fields) {
            if ((name == null || name == field.name)
            ) {
                return field
            }
        }
        searchType = searchType.superclass
    }
    return null
}

private fun getDeclaredFields(clazz: Class<*>): Array<Field> {
    return clazz.declaredFields
}
