package de.wiejack.kreator.builder.processor.api

import com.google.devtools.ksp.symbol.KSPropertyDeclaration
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.PropertySpec

interface PropertyInitializer {
    val order: Int
        get() = Int.MAX_VALUE

    fun couldInitialize(
        property: KSPropertyDeclaration,
        buildContext: BuildContext,
    ): Boolean

    fun initializeProperty(
        propertyDeclaration: KSPropertyDeclaration,
        buildContext: BuildContext,
        targetFile: FileSpec.Builder,
        propertyConsumer: (PropertySpec) -> Unit,
    )

}
