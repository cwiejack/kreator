package de.wiejack.kreator.builder.api

@BuilderDslMarker
class CollectionDsl<T>(
    val target: MutableCollection<T>,
) {
    operator fun invoke(config: CollectionDsl<T>.() -> Unit) {
        this.apply(config)
    }

    operator fun T.unaryPlus(): Boolean = target.add(this)

    infix fun add(toAdd: T) {
        target.add(toAdd)
    }
}

@BuilderDslMarker
class MapDsl<K, V>(
    val target: MutableMap<K, V>,
) {
    operator fun invoke(config: MapDsl<K, V>.() -> Unit) {
        this.apply(config)
    }

    operator fun Pair<K, V>.unaryPlus(): Boolean {
        target.put(this.first, this.second)
        return true
    }

    infix fun add(toAdd: Pair<K, V>) {
        target.put(toAdd.first, toAdd.second)
    }
}
