package de.wiejack.kreator.builder.api
@Retention(value = AnnotationRetention.SOURCE)
@Target(allowedTargets = [AnnotationTarget.PROPERTY])
annotation class PropertyInitializer(val initializerValue: String = "", val initializerString: String = "", val imports: Array<String> = [])
