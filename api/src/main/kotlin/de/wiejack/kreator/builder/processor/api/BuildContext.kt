package de.wiejack.kreator.builder.processor.api

import com.google.devtools.ksp.KspExperimental
import com.google.devtools.ksp.isAnnotationPresent
import com.google.devtools.ksp.processing.KSPLogger
import com.google.devtools.ksp.processing.Resolver
import com.google.devtools.ksp.symbol.KSClassDeclaration
import com.squareup.kotlinpoet.ksp.toClassName
import de.wiejack.kreator.builder.api.NoBuilder
import java.util.stream.Collectors.toList

@JvmInline
value class BuilderClassName(
    val name: String,
)

@JvmInline
value class SourceClassName(
    val name: String,
)

@JvmInline
value class PackageName(
    val name: String,
)

interface CommonKspContext {
    val resolver: Resolver
    val logger: KSPLogger
}

@OptIn(KspExperimental::class)
interface BuildContext : CommonKspContext {
    val sourceProperty: KSClassDeclaration
    val builderClassName: BuilderClassName
    val packageName: PackageName
    val initializeNullableProperties: Boolean
    val initializeCollectionsCount: Int
    val useCompanionObject: Boolean

    fun builderReference(): String =
        if (useCompanionObject) {
            sourceProperty.toClassName().canonicalName + ".builder {}"
        } else {
            sourceProperty.toClassName().simpleName + "Builder {}"
        }

    fun builderClassNameFor(classDeclaration: KSClassDeclaration): BuilderClassName

    fun buildableProperties() = sourceProperty.getAllProperties().filter { it.isAnnotationPresent(NoBuilder::class).not() }.toList()
}
