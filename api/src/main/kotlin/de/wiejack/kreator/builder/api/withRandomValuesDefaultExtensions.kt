package de.wiejack.kreator.builder.api

import java.math.BigDecimal
import java.sql.Timestamp
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.OffsetDateTime
import java.time.OffsetTime
import java.time.ZonedDateTime
import java.util.*
import kotlin.random.Random

@JvmName("withRandomValuesString")
fun CollectionDsl<in String>.withRandomValues(count: Int = 2) {
    target.addAll(
        (1..count).map { randomString(5) },
    )
}

@JvmName("withRandomValuesInt")
fun CollectionDsl<in Int>.withRandomValues(count: Int = 2) {
    target.addAll(
        (1..count).map { Random.nextInt() },
    )
}

@JvmName("withRandomValuesDouble")
fun CollectionDsl<in Double>.withRandomValues(count: Int = 2) {
    target.addAll(
        (1..count).map { Random.nextDouble() },
    )
}

@JvmName("withRandomValuesLong")
fun CollectionDsl<in Long>.withRandomValues(count: Int = 2) {
    target.addAll(
        (1..count).map { Random.nextLong() },
    )
}

@JvmName("withRandomValuesFloat")
fun CollectionDsl<in Float>.withRandomValues(count: Int = 2) {
    target.addAll(
        (1..count).map { Random.nextFloat() },
    )
}

@JvmName("withRandomValuesLocalDate")
fun CollectionDsl<in LocalDate>.withRandomValues(count: Int = 2) {
    target.addAll((1..count).map { LocalDate.now().minusDays(Random.nextLong(1, 100)) })
}

@JvmName("withRandomValuesLocalDateTime")
fun CollectionDsl<in LocalDateTime>.withRandomValues(count: Int = 2) {
    target.addAll((1..count).map { LocalDateTime.now().minusDays(Random.nextLong(1, 100)) })
}

@JvmName("withRandomValuesUUID")
fun CollectionDsl<in UUID>.withRandomValues(count: Int = 2) {
    target.addAll(
        (1..count).map { UUID.randomUUID() },
    )
}

@JvmName("withRandomValuesBigDecimal")
fun CollectionDsl<in BigDecimal>.withRandomValues(count: Int = 2) {
    target.addAll(
        (1..count).map { Random.nextInt().toBigDecimal() },
    )
}

@JvmName("withRandomValuesBoolean")
fun CollectionDsl<in Boolean>.withRandomValues(count: Int = 2) {
    target.addAll(
        (1..count).map { Random.nextBoolean() },
    )
}

@JvmName("withRandomValuesNumber")
fun CollectionDsl<in Number>.withRandomValues(count: Int = 2) {
    target.addAll((1..count).map { Random.nextInt() })
}

@JvmName("withRandomValuesOffsetDateTime")
fun CollectionDsl<in OffsetDateTime>.withRandomValues(count: Int = 2) {
    target.addAll((1..count).map { OffsetDateTime.now() })
}

@JvmName("withRandomValuesOffsetTime")
fun CollectionDsl<in OffsetTime>.withRandomValues(count: Int = 2) {
    target.addAll((1..count).map { OffsetTime.now() })
}

@JvmName("withRandomValuesShort")
fun CollectionDsl<in Short>.withRandomValues(count: Int = 2) {
    target.addAll((1..count).map { Random.nextInt().toShort() })
}

@JvmName("withRandomValuesTimestamp")
fun CollectionDsl<in Timestamp>.withRandomValues(count: Int = 2) {
    target.addAll((1..count).map { Timestamp.from(Instant.now()) })
}

@JvmName("withRandomValuesInstant")
fun CollectionDsl<in Instant>.withRandomValues(count: Int = 2) {
    target.addAll((1..count).map { Instant.now() })
}

@JvmName("withRandomValuesZonedDateTime")
fun CollectionDsl<in ZonedDateTime>.withRandomValues(count: Int = 2) {
    target.addAll((1..count).map { ZonedDateTime.now() })
}

@JvmName("withRandomValuesChar")
fun CollectionDsl<in Char>.withRandomValues(count: Int = 2) {
    target.addAll((1..count).map { Random.nextInt().toChar() })
}

@JvmName("withRandomValuesByte")
fun CollectionDsl<in Byte>.withRandomValues(count: Int = 2) {
    target.addAll((1..count).map { Random.nextInt().toByte() })
}

@JvmName("withRandomValuesByteArray")
fun CollectionDsl<in ByteArray>.withRandomValues(count: Int = 2) {
    target.addAll((1..count).map { Random.nextBytes(10) })
}

@JvmName("withRandomValuesLongArray")
fun CollectionDsl<in LongArray>.withRandomValues(count: Int = 2) {
    target.addAll((1..count).map { LongArray(10) { Random.nextLong() } })
}

@JvmName("withRandomValuesIntArray")
fun CollectionDsl<in IntArray>.withRandomValues(count: Int = 2) {
    target.addAll((1..count).map { IntArray(10) { Random.nextInt() } })
}

@JvmName("withRandomValuesShortArray")
fun CollectionDsl<in ShortArray>.withRandomValues(count: Int = 2) {
    target.addAll((1..count).map { ShortArray(10) { Random.nextInt().toShort() } })
}

@JvmName("withRandomValuesFloatArray")
fun CollectionDsl<in FloatArray>.withRandomValues(count: Int = 2) {
    target.addAll((1..count).map { FloatArray(10) { Random.nextFloat() } })
}

@JvmName("withRandomValuesDoubleArray")
fun CollectionDsl<in DoubleArray>.withRandomValues(count: Int = 2) {
    target.addAll((1..count).map { DoubleArray(10) { Random.nextDouble() } })
}

@JvmName("withRandomValuesBooleanArray")
fun CollectionDsl<in BooleanArray>.withRandomValues(count: Int = 2) {
    target.addAll((1..count).map { BooleanArray(10) { Random.nextBoolean() } })
}

@JvmName("withRandomValuesCurrency")
fun CollectionDsl<in Currency>.withRandomValues(count: Int = 2) {
    target.addAll(
        (1..count).map {
            Currency.getAvailableCurrencies().random()
        },
    )
}

@JvmName("withRandomValuesLocale")
fun CollectionDsl<in Locale>.withRandomValues(count: Int = 2) {
    target.addAll(
        (1..count).map {
            Locale.getAvailableLocales().random()
        },
    )
}
