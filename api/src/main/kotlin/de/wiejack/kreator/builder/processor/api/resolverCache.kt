package de.wiejack.kreator.builder.processor.api

import com.google.devtools.ksp.symbol.KSType
import com.google.devtools.ksp.symbol.KSTypeReference
import java.util.concurrent.ConcurrentHashMap

object ResolverCache : ConcurrentHashMap<KSTypeReference, KSType>() {

}

fun KSTypeReference.resolveCached(): KSType {
    return ResolverCache.getOrPut(this) { this.resolve() }
}