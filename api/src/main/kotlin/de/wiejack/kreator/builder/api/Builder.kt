package de.wiejack.kreator.builder.api

@Retention(value = AnnotationRetention.SOURCE)
@Target(allowedTargets = [AnnotationTarget.CLASS])
annotation class Builder(
    val useCompanionObject: Boolean = true,
    val initializeNullableProperties: Boolean = false,
    val initializeCollectionsCount: Int = 0,
)
