package de.wiejack.kreator.builder.api

@Retention(value = AnnotationRetention.SOURCE)
@Target(allowedTargets = [AnnotationTarget.PROPERTY])
annotation class InitializeCollectionCount(
    val count: Int,
)
