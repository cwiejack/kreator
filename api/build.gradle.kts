import org.gradle.api.tasks.testing.logging.TestLogEvent


plugins {
    kotlin("jvm")
    `maven-publish`
    signing
}


dependencies {
    compileOnly("com.google.devtools.ksp:symbol-processing-api:2.1.10-1.0.30")
    compileOnly("com.squareup:kotlinpoet-ksp:2.0.0")

    testImplementation(platform("org.junit:junit-bom:5.11.4"))
    testImplementation("org.junit.jupiter:junit-jupiter")
}


val javadocJar = tasks.register<Jar>("javadocJar") {
    archiveClassifier = "javadoc"
    from(tasks.javadoc)
}

val sourcesJar = tasks.register<Jar>("sourcesJar") {
    archiveClassifier = "sources"
    from(sourceSets.main.get().allSource)
}

tasks.withType(Test::class.java).configureEach {
    testLogging {
        events = setOf(TestLogEvent.PASSED, TestLogEvent.FAILED, TestLogEvent.SKIPPED)
    }

    useJUnitPlatform()
}

artifacts {
    archives(javadocJar)
    archives(sourcesJar)
}

publishing {
    publications {
        create<MavenPublication>("mavenPub") {
            groupId = project.group.toString()
            artifactId = "api"
            version = project.version.toString()
            from(components["kotlin"])
            artifact(sourcesJar)
            artifact(javadocJar)
            pom {
                name.set(project.name)
                description.set("Kotlin library for creating builder and copy functions.")
                url.set("https://gitlab.com/cwiejack/kreator")
                licenses {
                    license {
                        name.set("MIT License")
                        url.set("https://opensource.org/license/mit/")
                    }
                }
                developers {
                    developer {
                        id.set("cwiejack")
                        name.set("Christian Wiejack")
                        organization.set("")
                        organizationUrl.set("")
                    }
                }
                scm {
                    url.set(
                        "https://gitlab.com/cwiejack/kreator.git"
                    )
                    connection.set(
                        "scm:git:git://gitlab.com/cwiejack/kreator.git"
                    )
                    developerConnection.set(
                        "scm:git:git://gitlab.com/cwiejack/kreator.git"
                    )
                }
                issueManagement {
                    url.set("https://gitlab.com/cwiejack/kreator/-/issues")
                }
            }
        }
    }
    repositories {
        maven {
            val releasesRepoUrl = layout.buildDirectory.dir("repos/releases")
            val snapshotsRepoUrl = layout.buildDirectory.dir("repos/snapshots")
            url = uri(if (version.toString().endsWith("SNAPSHOT")) snapshotsRepoUrl else releasesRepoUrl)
        }
    }
}

signing {
    sign(publishing.publications["mavenPub"])
}


