package de.wiejack.kreator.builder.processor.propertyinitializer

import com.google.devtools.ksp.KspExperimental
import com.google.devtools.ksp.getAnnotationsByType
import com.google.devtools.ksp.symbol.KSPropertyDeclaration
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.PropertySpec
import com.squareup.kotlinpoet.ksp.toTypeName
import de.wiejack.kreator.builder.api.PropertyInitializer
import de.wiejack.kreator.builder.processor.api.BuildContext
import de.wiejack.kreator.builder.processor.api.resolveCached

@OptIn(KspExperimental::class)
class PropertyInitializerAnnotatedPropertyInitializer : de.wiejack.kreator.builder.processor.api.PropertyInitializer {

    override val order: Int
        get() = 10000

    override fun couldInitialize(
        property: KSPropertyDeclaration,
        buildContext: BuildContext,
    ): Boolean {
        return property.getAnnotationsByType(PropertyInitializer::class).count() > 0
    }

    override fun initializeProperty(
        propertyDeclaration: KSPropertyDeclaration,
        buildContext: BuildContext,
        targetFile: FileSpec.Builder,
        propertyConsumer: (PropertySpec) -> Unit,
    ) {
        val propertyInitializer = propertyDeclaration.getAnnotationsByType(PropertyInitializer::class).first()
        val initializer: String = propertyInitializer.initializerString.takeIf(String::isNotBlank)?.let { "\"$it\"" }
            ?: propertyInitializer.initializerValue

        propertyConsumer.invoke(
            PropertySpec.builder(
                propertyDeclaration.simpleName.asString(),
                propertyDeclaration.type.resolveCached().toTypeName()
            )
                .mutable(true)
                .initializer(initializer)
                .build()
        )

        propertyInitializer.imports.forEach {
            targetFile.addImport(it.substringBeforeLast("."), it.substringAfterLast("."))
        }
    }
}
