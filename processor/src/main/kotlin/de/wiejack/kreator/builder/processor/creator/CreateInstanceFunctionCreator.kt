package de.wiejack.kreator.builder.processor.creator

import com.google.devtools.ksp.KspExperimental
import com.google.devtools.ksp.getConstructors
import com.google.devtools.ksp.isAnnotationPresent
import com.google.devtools.ksp.symbol.KSPropertyDeclaration
import com.google.devtools.ksp.symbol.Modifier
import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.CodeBlock
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.LambdaTypeName
import com.squareup.kotlinpoet.ParameterSpec
import com.squareup.kotlinpoet.ksp.toClassName
import de.wiejack.kreator.builder.api.BuilderConstructor
import de.wiejack.kreator.builder.processor.api.BuildContext
import de.wiejack.kreator.builder.processor.api.resolveCached
import de.wiejack.kreator.builder.processor.propertyinitializer.CollectionInitializer
import de.wiejack.kreator.builder.processor.propertyinitializer.CollectionPropertyInitializer
import de.wiejack.kreator.builder.processor.propertyinitializer.OtherBuilderAnnotatedPropertyInitializer

@OptIn(KspExperimental::class)
class CreateInstanceFunctionCreator {
    companion object {
        const val CREATE_INSTANCE_FROM_EXTENSION_FUNCTION_NAME = "createInstance"
        const val CREATE_INSTANCE_FROM_BUILDER_FUNCTION_NAME = "createInstanceFromBuilder"
    }

    fun createInstanceFunction(
        targetFile: FileSpec.Builder,
        buildContext: BuildContext,
    ) {
        val packageName = buildContext.packageName
        val fqnClassName = buildContext.sourceProperty.toClassName().canonicalName
        val builderClassName = buildContext.builderClassName

        val createInstanceFunction =
            FunSpec
                .builder(CREATE_INSTANCE_FROM_EXTENSION_FUNCTION_NAME)
                .returns(ClassName(packageName.name, listOf(fqnClassName)))
                .addParameter(
                    ParameterSpec
                        .builder(
                            "block",
                            LambdaTypeName.get(
                                receiver = ClassName(packageName.name, builderClassName.name),
                                returnType = ClassName("kotlin", listOf("Unit")),
                            ),
                        ).build(),
                ).addCode(
                    CodeBlock
                        .builder()
                        .addStatement("val builderInstance = ${builderClassName.name}()")
                        .addStatement("block.invoke(builderInstance)")
                        .addStatement("""return $CREATE_INSTANCE_FROM_BUILDER_FUNCTION_NAME(builderInstance)""".trimIndent())
                        .build(),
                )

        targetFile.addFunction(createInstanceFunction.build())
    }

    fun createInstanceFromBuilderFunction(
        targetFile: FileSpec.Builder,
        buildContext: BuildContext,
    ) {
        val packageName = buildContext.packageName
        val fqnClassName = buildContext.sourceProperty.toClassName().canonicalName
        val builderClassName = buildContext.builderClassName

        val constructorParams = findConstructor(buildContext).parameters

        val (constructorProperties, remainingProperties) =
            buildContext
                .buildableProperties()
                .partition { prop -> constructorParams.any { cProp -> cProp.name?.asString() == prop.simpleName.asString() } }
        val (withSetterProperties, noSetterProperties) = remainingProperties.partition(::hasVisibleSetter)

        val instanceCreationString =
            """
            $fqnClassName(
                ${initializeConstructorProperties(constructorProperties, buildContext)}
            )
            ${initializeSetterProperties(withSetterProperties, buildContext)}
            ${initializeWithReflection(noSetterProperties, targetFile, buildContext)}
            """.trimIndent()

        val createInstanceFunction =
            FunSpec
                .builder(CREATE_INSTANCE_FROM_BUILDER_FUNCTION_NAME)
                .returns(ClassName(packageName.name, listOf(fqnClassName)))
                .addParameter(
                    ParameterSpec
                        .builder(
                            "builderInstance",
                            ClassName(packageName.name, builderClassName.name),
                        ).build(),
                ).addCode(
                    CodeBlock
                        .builder()
                        .addStatement("""return $instanceCreationString""".trimIndent())
                        .build(),
                )

        targetFile.addFunction(createInstanceFunction.build())
    }

    private fun initializeWithReflection(
        noSetterProperties: List<KSPropertyDeclaration>,
        fileType: FileSpec.Builder,
        buildContext: BuildContext,
    ): String =
        if (noSetterProperties.isNotEmpty()) {
            fileType.addImport("de.wiejack.kreator", "findField")

            val reflectionValues =
                noSetterProperties.joinToString("\n") { property ->
                    """
                    findField(${buildContext.sourceProperty.qualifiedName!!.asString()}::class.java, "${property.simpleName.asString()}")?.run{
                        this.isAccessible = true
                        set(this@apply, ${initializePropertyFromBuilder(property, buildContext)})
                    }
                    """.trimIndent()
                }
            """
            .apply {
                $reflectionValues
            }
            """.trimIndent()
        } else {
            ""
        }

    private fun initializeSetterProperties(
        setterProperties: List<KSPropertyDeclaration>,
        buildContext: BuildContext,
    ): String =
        if (setterProperties.isNotEmpty()) {
            val setProperties =
                setterProperties.joinToString("\n") { property ->
                    "${property.simpleName.asString()} = ${initializePropertyFromBuilder(property, buildContext)}"
                }
            """
            .apply {
                $setProperties
            }
            """.trimIndent()
        } else {
            ""
        }

    private fun initializeConstructorProperties(
        constructorProperties: List<KSPropertyDeclaration>,
        buildContext: BuildContext,
    ): String =
        constructorProperties.joinToString(",\n") { property ->
            "${property.simpleName.asString()} = ${initializePropertyFromBuilder(property, buildContext)}"
        }

    private fun findConstructor(buildContext: BuildContext) =
        (
            buildContext.sourceProperty.getConstructors().firstOrNull { it.isAnnotationPresent(BuilderConstructor::class) }
                ?: buildContext.sourceProperty.primaryConstructor
                ?: throw IllegalStateException(
                    "${buildContext.sourceProperty.qualifiedName?.asString()} must have either a @BuilderConstructor annotated constructor or a primary constructor.",
                )
        )

    private fun hasVisibleSetter(property: KSPropertyDeclaration) =
        (property.setter != null) and (
            (
                Modifier.PUBLIC in (
                    property.setter?.modifiers
                        ?: emptySet()
                )
            ) or (Modifier.INTERNAL in (property.setter?.modifiers ?: emptySet()))
        )

    private fun initializePropertyFromBuilder(
        property: KSPropertyDeclaration,
        buildContext: BuildContext,
    ): CharSequence {
        val collectionInitializer =
            lazy {
                CollectionPropertyInitializer.resolveCollectionInitializer(
                    property,
                    buildContext,
                )
            }
        return when {
            isAnnotatedWithBuilder(property, buildContext) -> {
                if (property.type.resolveCached().isMarkedNullable) {
                    val nullInitializer =
                        "${property.type.resolveCached().declaration.packageName.asString()}.${CREATE_INSTANCE_FROM_EXTENSION_FUNCTION_NAME}(this)"
                    "builderInstance.${property.simpleName.asString()}?.run { $nullInitializer }"
                } else {
                    "${property.type.resolveCached().declaration.packageName.asString()}.${CREATE_INSTANCE_FROM_BUILDER_FUNCTION_NAME}(builderInstance.${property.simpleName.asString()})"
                }
            }

            collectionInitializer.value !is CollectionInitializer.NoInitialization -> {
                if (property.type.resolveCached().isMarkedNullable && buildContext.initializeNullableProperties.not()) {
                    "builderInstance.${property.simpleName.asString()}?.let { builderInstance.${
                        collectionInitializer.value.getPrivatePropertyName(
                            property,
                        )
                    } }"
                } else {
                    "builderInstance.${collectionInitializer.value.getPrivatePropertyName(property)}"
                }
            }

            else -> {
                "builderInstance.${property.simpleName.asString()}"
            }
        }
    }

    private fun isAnnotatedWithBuilder(
        property: KSPropertyDeclaration,
        buildContext: BuildContext,
    ): Boolean = OtherBuilderAnnotatedPropertyInitializer.isBuilderAnnotatedType(property.type, buildContext)
}
