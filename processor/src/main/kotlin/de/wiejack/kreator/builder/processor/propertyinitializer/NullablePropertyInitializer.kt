package de.wiejack.kreator.builder.processor.propertyinitializer

import com.google.devtools.ksp.symbol.KSPropertyDeclaration
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.PropertySpec
import com.squareup.kotlinpoet.ksp.toTypeName
import de.wiejack.kreator.builder.processor.api.BuildContext
import de.wiejack.kreator.builder.processor.api.CommonKspContext
import de.wiejack.kreator.builder.processor.api.PropertyInitializer
import de.wiejack.kreator.builder.processor.api.resolveCached
import de.wiejack.kreator.builder.processor.propertyinitializer.OtherBuilderAnnotatedPropertyInitializer.Companion.isBuilderAnnotatedType

class NullablePropertyInitializer : PropertyInitializer {
    override fun couldInitialize(
        property: KSPropertyDeclaration,
        buildContext: BuildContext,
    ): Boolean {
        if (buildContext.initializeNullableProperties) {
            return false
        }
        //Special treatment for @Builder annotated property in explicit PropertyInitializer
        return property.type.resolveCached().isMarkedNullable && !isBuilderAnnotatedType(
            type = property.type,
            commonKspContext = buildContext
        )
    }

    override fun initializeProperty(
        propertyDeclaration: KSPropertyDeclaration,
        buildContext: BuildContext,
        targetFile: FileSpec.Builder,
        propertyConsumer: (PropertySpec) -> Unit,
    ) {
        propertyConsumer.invoke(
            PropertySpec.Companion.builder(
                propertyDeclaration.simpleName.asString(),
                propertyDeclaration.type.toTypeName()
            )
                .mutable(true)
                .initializer("null")
                .build()
        )
    }

    override val order: Int
        get() = 11001
}
