package de.wiejack.kreator.builder.processor.propertyinitializer

import com.google.devtools.ksp.symbol.ClassKind
import com.google.devtools.ksp.symbol.KSClassDeclaration
import com.google.devtools.ksp.symbol.KSPropertyDeclaration
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.PropertySpec
import com.squareup.kotlinpoet.ksp.toTypeName
import de.wiejack.kreator.builder.processor.api.BuildContext
import de.wiejack.kreator.builder.processor.api.PropertyInitializer
import de.wiejack.kreator.builder.processor.api.resolveCached

class EnumPropertyInitializer : PropertyInitializer {
    override fun couldInitialize(
        property: KSPropertyDeclaration,
        buildContext: BuildContext,
    ): Boolean {
        val classDeclaration = (property.type.resolveCached().declaration as KSClassDeclaration)
        return classDeclaration.classKind == ClassKind.ENUM_CLASS
    }

    override fun initializeProperty(
        propertyDeclaration: KSPropertyDeclaration,
        buildContext: BuildContext,
        targetFile: FileSpec.Builder,
        propertyConsumer: (PropertySpec) -> Unit,
    ) {
        val enumTypeName = propertyDeclaration.type.toTypeName()

        val enumType =
            if (buildContext.initializeNullableProperties) {
                enumTypeName.copy(nullable = false)
            } else {
                enumTypeName
            }
        val isNullable = propertyDeclaration.type.resolveCached().isMarkedNullable
        val initializer =
            if (isNullable && buildContext.initializeNullableProperties.not()) {
                "null"
            } else {
                "$enumType.entries[0]"
            }

        val propertyType =
            PropertySpec.Companion
                .builder(propertyDeclaration.simpleName.asString(), enumTypeName)
                .mutable(true)
                .initializer(initializer)

        propertyConsumer.invoke(propertyType.build())
    }

    override val order: Int
        get() = 12000
}
