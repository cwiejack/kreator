package de.wiejack.kreator.builder.processor.creator

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.LambdaTypeName
import com.squareup.kotlinpoet.ParameterSpec
import com.squareup.kotlinpoet.ksp.toClassName
import de.wiejack.kreator.builder.processor.api.BuildContext

class BuilderFunctionCreator {
    companion object {
        private const val EXTENSION_FUNCTION_NAME = "builder"
    }

    fun createFunction(
        targetFile: FileSpec.Builder,
        buildContext: BuildContext,
    ) {
        val functionBuilder =
            if (buildContext.useCompanionObject) {
                createExtensionFunction(buildContext)
            } else {
                createRootFunction(buildContext)
            }

        targetFile.addFunction(functionBuilder.build())
    }

    private fun createRootFunction(buildContext: BuildContext): FunSpec.Builder {
        val packageName = buildContext.packageName.name
        val simpleName = buildContext.sourceProperty.toClassName().simpleName
        val fqnClassName = buildContext.sourceProperty.toClassName().canonicalName

        val builderFunctionName = "${simpleName}Builder"
        val builderClassName = buildContext.builderClassName.name

        val rootFunction =
            FunSpec
                .builder(builderFunctionName)
                .returns(ClassName(packageName, listOf(fqnClassName)))
                .addParameter(
                    ParameterSpec
                        .builder(
                            "block",
                            LambdaTypeName.get(
                                receiver = ClassName(packageName, builderClassName),
                                returnType = ClassName("kotlin", listOf("Unit")),
                            ),
                        ).build(),
                ).addStatement("return ${CreateInstanceFunctionCreator.CREATE_INSTANCE_FROM_EXTENSION_FUNCTION_NAME}(block)")

        return rootFunction
    }

    private fun createExtensionFunction(buildContext: BuildContext): FunSpec.Builder {
        val packageName = buildContext.packageName.name
        val fqnClassName = buildContext.sourceProperty.toClassName().canonicalName
        val builderClassName = buildContext.builderClassName.name

        val extensionFunction =
            FunSpec
                .builder(EXTENSION_FUNCTION_NAME)
                .receiver(ClassName(packageName, listOf("$fqnClassName.Companion")))
                .returns(ClassName(packageName, listOf(fqnClassName)))
                .addParameter(
                    ParameterSpec
                        .builder(
                            "block",
                            LambdaTypeName.get(
                                receiver = ClassName(packageName, builderClassName),
                                returnType = ClassName("kotlin", listOf("Unit")),
                            ),
                        ).build(),
                ).addStatement("return ${CreateInstanceFunctionCreator.CREATE_INSTANCE_FROM_EXTENSION_FUNCTION_NAME}(block)")
        return extensionFunction
    }
}
