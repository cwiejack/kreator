package de.wiejack.kreator.builder.processor.propertyinitializer

import com.google.devtools.ksp.KspExperimental
import com.google.devtools.ksp.getAnnotationsByType
import com.google.devtools.ksp.getClassDeclarationByName
import com.google.devtools.ksp.processing.Resolver
import com.google.devtools.ksp.symbol.KSPropertyDeclaration
import com.google.devtools.ksp.symbol.KSTypeReference
import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.KModifier
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import com.squareup.kotlinpoet.PropertySpec
import com.squareup.kotlinpoet.TypeName
import com.squareup.kotlinpoet.ksp.toTypeName
import de.wiejack.kreator.builder.api.InitializeCollectionCount
import de.wiejack.kreator.builder.processor.api.BuildContext
import de.wiejack.kreator.builder.processor.api.PropertyInitializer
import de.wiejack.kreator.builder.processor.api.resolveCached
import de.wiejack.kreator.builder.processor.propertyinitializer.CollectionPropertyInitializer.Companion.resolveCollectionInitializer
import kotlin.reflect.KClass

class CollectionPropertyInitializer : PropertyInitializer {
    companion object {
        @OptIn(KspExperimental::class)
        fun resolveCollectionInitializer(
            property: KSPropertyDeclaration,
            buildContext: BuildContext,
        ): CollectionInitializer {
            val resolver = buildContext.resolver
            val propertyStarProjection =
                property.type
                    .resolveCached()
                    .makeNotNullable()
                    .starProjection()

            return when {
                starProjected(resolver, List::class).isAssignableFrom(propertyStarProjection) -> {
                    CollectionInitializer.ListInitialization
                }

                starProjected(resolver, Set::class).isAssignableFrom(propertyStarProjection) -> {
                    CollectionInitializer.SetInitialization
                }

                starProjected(resolver, Map::class).isAssignableFrom(propertyStarProjection) -> {
                    CollectionInitializer.MapInitialization
                }

                else -> {
                    CollectionInitializer.NoInitialization
                }
            }
        }

        private fun starProjected(
            resolver: Resolver,
            clazz: KClass<*>,
        ) = (
            resolver.getClassDeclarationByName(clazz.java.canonicalName)?.asStarProjectedType()
                ?: error("Expecting to resolve type: ${clazz.java.canonicalName}")
        )
    }

    override fun couldInitialize(
        property: KSPropertyDeclaration,
        buildContext: BuildContext,
    ): Boolean = resolveCollectionInitializer(property, buildContext).couldInitialize()

    override fun initializeProperty(
        propertyDeclaration: KSPropertyDeclaration,
        buildContext: BuildContext,
        targetFile: FileSpec.Builder,
        propertyConsumer: (PropertySpec) -> Unit,
    ) {
        resolveCollectionInitializer(propertyDeclaration, buildContext).initializeProperty(
            propertyDeclaration = propertyDeclaration,
            buildContext = buildContext,
            targetFile = targetFile,
            propertyConsumer = propertyConsumer,
        )
    }

    override val order: Int
        get() = 11000
}

sealed class CollectionInitializer {
    abstract fun couldInitialize(): Boolean

    open fun initializeProperty(
        propertyDeclaration: KSPropertyDeclaration,
        buildContext: BuildContext,
        targetFile: FileSpec.Builder,
        propertyConsumer: (PropertySpec) -> Unit,
    ) {
        val collectionInitializer: CollectionInitializer =
            resolveCollectionInitializer(propertyDeclaration, buildContext)

        val privatePropertyName = getPrivatePropertyName(propertyDeclaration)

        val privateCollectionProperty =
            PropertySpec
                .builder(privatePropertyName, collectionInitializer.mutableTypeName(propertyDeclaration))
                .mutable(false)
                .addModifiers(KModifier.INTERNAL)
                .initializer(collectionInitializer.defaultInitializer(propertyDeclaration))

        val collectionDSLProperty =
            createCollectionDSLProperty(
                targetFile = targetFile,
                propertyDeclaration = propertyDeclaration,
                privatePropertyName = privatePropertyName,
                collectionInitializer = collectionInitializer,
                ignoreNullability = buildContext.initializeNullableProperties,
                initializeCollectionCount = buildContext.initializeCollectionsCount,
            )

        propertyConsumer.invoke(privateCollectionProperty.build())
        propertyConsumer.invoke(collectionDSLProperty.build())
    }

    abstract fun mutableTypeName(propertyDeclaration: KSPropertyDeclaration): TypeName

    abstract fun defaultInitializer(propertyDeclaration: KSPropertyDeclaration): String

    @OptIn(KspExperimental::class)
    private fun createCollectionDSLProperty(
        targetFile: FileSpec.Builder,
        propertyDeclaration: KSPropertyDeclaration,
        collectionInitializer: CollectionInitializer,
        privatePropertyName: String,
        ignoreNullability: Boolean,
        initializeCollectionCount: Int,
    ): PropertySpec.Builder {
        val collectionCount =
            propertyDeclaration.getAnnotationsByType(InitializeCollectionCount::class).firstOrNull()?.count
                ?: initializeCollectionCount

        val prefilledInitializer =
            if (collectionCount > 0) {
                targetFile.addImport("de.wiejack.kreator.builder.api", "withRandomValues")
                ".apply { withRandomValues($collectionCount) }"
            } else {
                ""
            }
        return if (ignoreNullability or
            propertyDeclaration.type
                .resolveCached()
                .isMarkedNullable
                .not()
        ) {
            if (collectionInitializer is CollectionInitializer.MapInitialization) {
                val initializerString = "MapDsl($privatePropertyName)$prefilledInitializer"
                createCollectionMapPropertySpecBuilder(
                    propertyDeclaration = propertyDeclaration,
                    mutable = ignoreNullability,
                    makeNullable = false,
                    initializerString = initializerString,
                )
            } else {
                val initializerString = "CollectionDsl($privatePropertyName)$prefilledInitializer"
                createCollectionPropertySpecBuilder(
                    propertyDeclaration = propertyDeclaration,
                    mutable = ignoreNullability,
                    makeNullable = false,
                    initializerString = initializerString,
                )
            }
        } else {
            if (collectionInitializer is CollectionInitializer.MapInitialization) {
                createCollectionMapPropertySpecBuilder(
                    propertyDeclaration = propertyDeclaration,
                    mutable = true,
                    makeNullable = true,
                    initializerString = "null",
                )
            } else {
                createCollectionPropertySpecBuilder(
                    propertyDeclaration = propertyDeclaration,
                    mutable = true,
                    makeNullable = true,
                    initializerString = "null",
                )
            }
        }
    }

    private fun createCollectionPropertySpecBuilder(
        propertyDeclaration: KSPropertyDeclaration,
        mutable: Boolean,
        makeNullable: Boolean,
        initializerString: String,
    ): PropertySpec.Builder =
        PropertySpec
            .builder(
                propertyDeclaration.simpleName.asString(),
                ClassName("de.wiejack.kreator.builder.api", "CollectionDsl")
                    .parameterizedBy(
                        genericTypeNames(propertyDeclaration),
                    ).copy(nullable = makeNullable),
            ).mutable(mutable)
            .initializer(initializerString)

    private fun createCollectionMapPropertySpecBuilder(
        propertyDeclaration: KSPropertyDeclaration,
        mutable: Boolean,
        makeNullable: Boolean,
        initializerString: String,
    ): PropertySpec.Builder =
        PropertySpec
            .builder(
                propertyDeclaration.simpleName.asString(),
                ClassName("de.wiejack.kreator.builder.api", "MapDsl")
                    .parameterizedBy(
                        genericTypeNames(propertyDeclaration),
                    ).copy(nullable = makeNullable),
            ).mutable(mutable)
            .initializer(initializerString)

    protected fun genericTypes(propertyDeclaration: KSPropertyDeclaration): List<KSTypeReference> =
        propertyDeclaration.type.element
            ?.typeArguments!!
            .map { it.type!! }

    protected fun genericTypeNames(propertyDeclaration: KSPropertyDeclaration): List<TypeName> =
        genericTypes(propertyDeclaration).map {
            it.toTypeName()
        }

    fun getPrivatePropertyName(property: KSPropertyDeclaration): String = CollectionInitializer.getPrivatePropertyName(property)

    data object NoInitialization : CollectionInitializer() {
        override fun couldInitialize() = false

        override fun initializeProperty(
            propertyDeclaration: KSPropertyDeclaration,
            buildContext: BuildContext,
            targetFile: FileSpec.Builder,
            propertyConsumer: (PropertySpec) -> Unit,
        ): Unit = throw NotImplementedError("no implementation")

        override fun mutableTypeName(propertyDeclaration: KSPropertyDeclaration): TypeName = throw NotImplementedError("no implementation")

        override fun defaultInitializer(propertyDeclaration: KSPropertyDeclaration): String = throw NotImplementedError("no implementation")
    }

    data object ListInitialization : CollectionInitializer() {
        override fun couldInitialize() = true

        override fun mutableTypeName(propertyDeclaration: KSPropertyDeclaration): TypeName =
            ClassName("kotlin.collections", "MutableList").parameterizedBy(genericTypeNames(propertyDeclaration))

        override fun defaultInitializer(propertyDeclaration: KSPropertyDeclaration): String {
            val typesJoined =
                propertyDeclaration.type.element
                    ?.typeArguments
                    ?.joinToString {
                        it.type
                            ?.resolveCached()
                            ?.declaration
                            ?.qualifiedName
                            ?.asString()
                            .orEmpty()
                    }.orEmpty()
            return "mutableListOf<$typesJoined>()"
        }
    }

    data object SetInitialization : CollectionInitializer() {
        override fun couldInitialize() = true

        override fun mutableTypeName(propertyDeclaration: KSPropertyDeclaration): TypeName =
            ClassName("kotlin.collections", "MutableSet").parameterizedBy(genericTypeNames(propertyDeclaration))

        override fun defaultInitializer(propertyDeclaration: KSPropertyDeclaration): String {
            val typesJoined =
                propertyDeclaration.type.element?.typeArguments?.joinToString {
                    it.type
                        ?.resolveCached()
                        ?.declaration
                        ?.qualifiedName
                        ?.asString()
                        .orEmpty()
                } ?: ""
            return "mutableSetOf<$typesJoined>()"
        }
    }

    companion object {
        fun getPrivatePropertyName(propertyDeclaration: KSPropertyDeclaration): String = "_${propertyDeclaration.simpleName.asString()}"
    }

    data object MapInitialization : CollectionInitializer() {
        override fun couldInitialize(): Boolean = true

        override fun mutableTypeName(propertyDeclaration: KSPropertyDeclaration): TypeName =
            ClassName("kotlin.collections", "MutableMap").parameterizedBy(genericTypeNames(propertyDeclaration))

        override fun defaultInitializer(propertyDeclaration: KSPropertyDeclaration): String {
            val typesJoined =
                propertyDeclaration.type.element?.typeArguments?.joinToString {
                    it.type
                        ?.resolveCached()
                        ?.declaration
                        ?.qualifiedName
                        ?.asString()
                        .orEmpty()
                } ?: ""
            return "mutableMapOf<$typesJoined>()"
        }
    }
}
