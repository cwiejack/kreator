package de.wiejack.kreator.builder.processor

import com.google.devtools.ksp.KspExperimental
import com.google.devtools.ksp.getAnnotationsByType
import com.google.devtools.ksp.processing.CodeGenerator
import com.google.devtools.ksp.processing.KSPLogger
import com.google.devtools.ksp.processing.Resolver
import com.google.devtools.ksp.processing.SymbolProcessor
import com.google.devtools.ksp.symbol.KSAnnotated
import com.google.devtools.ksp.symbol.KSClassDeclaration
import com.google.devtools.ksp.symbol.KSFile
import com.google.devtools.ksp.symbol.KSPropertyDeclaration
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.ksp.kspDependencies
import com.squareup.kotlinpoet.ksp.originatingKSFiles
import de.wiejack.kreator.builder.api.Builder
import de.wiejack.kreator.builder.processor.api.CommonKspContext
import de.wiejack.kreator.builder.processor.api.PropertyInitializer
import de.wiejack.kreator.builder.processor.creator.BuilderClassCreator
import de.wiejack.kreator.builder.processor.creator.BuilderFunctionCreator
import de.wiejack.kreator.builder.processor.creator.CollectionDslCreator
import de.wiejack.kreator.builder.processor.creator.CreateInstanceFunctionCreator
import de.wiejack.kreator.builder.processor.creator.NullPropertyInitFunctionCreator
import java.io.OutputStreamWriter
import java.nio.charset.StandardCharsets

class BuilderSymbolProcessor(
    private val codeGenerator: CodeGenerator,
    private val logger: KSPLogger,
) : SymbolProcessor {

    private val builderClassCreator = BuilderClassCreator()
    private val createInstanceFunctionCreator = CreateInstanceFunctionCreator()
    private val builderFunctionCreator = BuilderFunctionCreator()
    private val collectionDslCreator = CollectionDslCreator()
    private val nullPropertyInitFunctionCreator = NullPropertyInitFunctionCreator()

    override fun process(resolver: Resolver): List<KSAnnotated> {
        logger.info("Starting Kreator Builder Processor...")
        val commonKspContext = CommonKSPContextImpl(
            resolver = resolver,
            logger = logger
        )
        val allToBuild: Sequence<KSClassDeclaration> = resolveAllBuilderDeclarations(resolver)
        val allPropertyInitializers: List<PropertyInitializer> = PropertyInitializerResolver.allPropertyInitializer()
        val (processable, notYetProcessable) = partitionToProcessableAndNotYetProcessable(
            allToBuild,
            allPropertyInitializers,
            commonKspContext
        )

        processable.forEach { toBuildClassDeclaration ->
            createBuilderFile(toBuildClassDeclaration, allPropertyInitializers, resolver)
        }

        return notYetProcessable
    }

    private fun createBuilderFile(
        toBuildClassDeclaration: KSClassDeclaration,
        propertyInitializers: List<PropertyInitializer>,
        resolver: Resolver
    ) {
        val buildContext = initializeBuildContext(toBuildClassDeclaration, resolver)
        FileSpec.builder(buildContext.packageName.name, buildContext.builderClassName.name)
            .also { fileBuilder ->
                builderClassCreator.create(fileBuilder, propertyInitializers, buildContext)
                createInstanceFunctionCreator.createInstanceFunction(fileBuilder, buildContext)
                createInstanceFunctionCreator.createInstanceFromBuilderFunction(fileBuilder, buildContext)
                builderFunctionCreator.createFunction(fileBuilder, buildContext)
                collectionDslCreator.createExtensionFunction(
                    fileBuilder,
                    toBuildClassDeclaration,
                    buildContext
                )
                nullPropertyInitFunctionCreator.createInitializationFunction(
                    fileBuilder,
                    toBuildClassDeclaration,
                    buildContext
                )
            }
            .build()
            .write(codeGenerator, false, listOfNotNull(toBuildClassDeclaration.containingFile))
    }

    private fun partitionToProcessableAndNotYetProcessable(
        allToBuild: Sequence<KSClassDeclaration>,
        allPropertyInitializers: List<PropertyInitializer>,
        commonKspContext: CommonKspContext
    ): Pair<List<KSClassDeclaration>, List<KSClassDeclaration>> = allToBuild.partition { toBuildClassDeclaration ->
        val buildContext = initializeBuildContext(toBuildClassDeclaration, commonKspContext.resolver)
        val classProperties: List<KSPropertyDeclaration> = toBuildClassDeclaration.getAllProperties().toList()
        classProperties.all { property ->
            allPropertyInitializers.any { builder -> builder.couldInitialize(property, buildContext) }
        }
    }

    @OptIn(KspExperimental::class)
    private fun initializeBuildContext(
        classProperty: KSClassDeclaration,
        resolver: Resolver
    ): BuildContextImpl {
        val builderAnnotation = classProperty.getAnnotationsByType(Builder::class).first()
        return BuildContextImpl(
            sourceProperty = classProperty,
            initializeNullableProperties = builderAnnotation.initializeNullableProperties,
            initializeCollectionsCount = builderAnnotation.initializeCollectionsCount,
            useCompanionObject = builderAnnotation.useCompanionObject,
            resolver = resolver,
            logger = logger
        )
    }

    private fun resolveAllBuilderDeclarations(resolver: Resolver) = resolver
        .getSymbolsWithAnnotation(Builder::class.java.canonicalName)
        .filterIsInstance<KSClassDeclaration>()

    private fun FileSpec.write(
        codeGenerator: CodeGenerator,
        aggregating: Boolean,
        originatingKSFiles: Iterable<KSFile> = originatingKSFiles()
    ) {
        val dependencies = kspDependencies(aggregating, originatingKSFiles)
        val file =
            codeGenerator.createNewFileByPath(dependencies, "kreator/builder/${packageName.replace(".", "/")}/$name")
        // Don't use writeTo(file) because that tries to handle directories under the hood
        OutputStreamWriter(file, StandardCharsets.UTF_8)
            .use(::writeTo)
    }
}
