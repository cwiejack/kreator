package de.wiejack.kreator.builder.processor

import com.google.devtools.ksp.symbol.KSClassDeclaration
import com.google.devtools.ksp.symbol.KSPropertyDeclaration
import com.google.devtools.ksp.symbol.KSTypeReference
import de.wiejack.kreator.builder.processor.api.resolveCached


fun propertyClassDeclaration(propertyDeclaration: KSPropertyDeclaration) =
    classDeclaration(propertyDeclaration.type)

fun classDeclaration(typeReference: KSTypeReference) = typeReference.resolveCached().declaration as KSClassDeclaration

