package de.wiejack.kreator.builder.processor

import com.google.devtools.ksp.KspExperimental
import de.wiejack.kreator.builder.processor.api.PropertyInitializer
import de.wiejack.kreator.builder.processor.propertyinitializer.AbstractPropertyInitializer
import de.wiejack.kreator.builder.processor.propertyinitializer.CollectionPropertyInitializer
import de.wiejack.kreator.builder.processor.propertyinitializer.EnumPropertyInitializer
import de.wiejack.kreator.builder.processor.propertyinitializer.NullablePropertyInitializer
import de.wiejack.kreator.builder.processor.propertyinitializer.OtherBuilderAnnotatedPropertyInitializer
import de.wiejack.kreator.builder.processor.propertyinitializer.PropertyInitializerAnnotatedPropertyInitializer
import java.util.ServiceLoader

object PropertyInitializerResolver {
    fun allPropertyInitializer(): List<PropertyInitializer> {
        val allInitializer =
            listOf(
                PropertyInitializerAnnotatedPropertyInitializer(),
                NullablePropertyInitializer(),
                EnumPropertyInitializer(),
                CollectionPropertyInitializer(),
                OtherBuilderAnnotatedPropertyInitializer(),
            ) + AbstractPropertyInitializer.allSimplePropertyInitializer() + propertyInitializer()

        return allInitializer.sortedBy(PropertyInitializer::order)
    }

    @OptIn(KspExperimental::class)
    private fun propertyInitializer(): List<PropertyInitializer> =
        ServiceLoader
            .load(
                de.wiejack.kreator.builder.processor.api.PropertyInitializer::class.java,
                de.wiejack.kreator.builder.processor.api.PropertyInitializer::class.java.classLoader,
            ).stream()
            .map { it.get() }
            .toList()
}
