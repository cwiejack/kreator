package de.wiejack.kreator.builder.processor.creator

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.KModifier
import com.squareup.kotlinpoet.LambdaTypeName
import com.squareup.kotlinpoet.ParameterSpec
import com.squareup.kotlinpoet.TypeName
import com.squareup.kotlinpoet.TypeSpec
import de.wiejack.kreator.builder.api.BuilderDslMarker
import de.wiejack.kreator.builder.processor.api.BuildContext
import de.wiejack.kreator.builder.processor.api.PropertyInitializer

class BuilderClassCreator {

    fun create(targetFile: FileSpec.Builder, propertyCreators: List<PropertyInitializer>, buildContext: BuildContext) {
        val builderType = TypeSpec.classBuilder(buildContext.builderClassName.name).apply {
            addAnnotation(BuilderDslMarker::class)
            buildContext.buildableProperties().forEach { property ->
                propertyCreators
                    .first { creator -> creator.couldInitialize(property, buildContext) }
                    .initializeProperty(property, buildContext, targetFile) { initProperty ->
                        this.addProperty(initProperty)
                    }
            }
            /*
            operator fun invoke(customizer: SecondIdeaBuilder.() -> Unit) {
        this.apply(customizer)
    }

    .receiver(ClassName(packageName, listOf("$fqnClassName.Companion")))
             */
            val customizerParam = ParameterSpec.builder("customizer", LambdaTypeName.get(
                receiver = ClassName("", buildContext.builderClassName.name),
                returnType = ClassName("kotlin", listOf("Unit"))
            )).build()
            addFunction(FunSpec.builder("invoke")
                .addParameter(customizerParam)
                .addCode("this.apply(customizer)")
                .addModifiers(KModifier.OPERATOR)
                .build())
        }

        targetFile.addType(builderType.build())
    }
}
