package de.wiejack.kreator.builder.processor.creator

import com.google.devtools.ksp.symbol.KSClassDeclaration
import com.squareup.kotlinpoet.AnnotationSpec
import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.FunSpec
import de.wiejack.kreator.builder.processor.BuildContextImpl
import de.wiejack.kreator.builder.processor.PropertyInitializerResolver
import de.wiejack.kreator.builder.processor.api.BuildContext
import de.wiejack.kreator.builder.processor.api.resolveCached
import de.wiejack.kreator.builder.processor.propertyinitializer.CollectionInitializer
import de.wiejack.kreator.builder.processor.propertyinitializer.NullablePropertyInitializer

class NullPropertyInitFunctionCreator {

    private val propertyInitializers =
        PropertyInitializerResolver.allPropertyInitializer()
            .filter { it !is NullablePropertyInitializer }

    internal fun createInitializationFunction(
        targetFile: FileSpec.Builder,
        classDeclaration: KSClassDeclaration,
        buildContext: BuildContext,
    ) {
        val initializeFunctionBuildContext =
            (buildContext as BuildContextImpl).copy(initializeNullableProperties = true)
        val type = classDeclaration.asType(emptyList())
        val packageName = initializeFunctionBuildContext.packageName.name
        val builderClassName = initializeFunctionBuildContext.builderClassName.name

        targetFile.addImport(packageName, "builder")
        val initializeFunction = FunSpec.builder(INITIALIZE_NULL_PROPERTIES_FUNCTION_NAME)
            .receiver(ClassName(packageName, builderClassName))
//            .addParameter(ParameterSpec.builder("count", Int::class).defaultValue("2").build())
            .addAnnotation(
                AnnotationSpec.builder(JvmName::class)
                    .addMember("\"$INITIALIZE_NULL_PROPERTIES_FUNCTION_NAME${type.declaration.simpleName.asString()}\"")
                    .build()
            )



        initializeFunctionBuildContext.buildableProperties()
            .filter { it.type.resolveCached().isMarkedNullable }
            .forEach { property ->
                val initializer = propertyInitializers.firstOrNull { pI ->
                    pI.couldInitialize(property, initializeFunctionBuildContext)
                }

                if (initializer == null) {
                    error("No initializer found for property ${property.simpleName.asString()}")
                }

                initializer.initializeProperty(
                    property,
                    initializeFunctionBuildContext,
                    targetFile
                ) {
                    val propInitString = it.toString()
                        .trim()
                        .replace(valvarRegex, "")
                        .replace(valvarRegexWithVisibilityPrefix, "")
                        .replace(typeDefinitionRegex, " = ")
                    val privatePropertyName = CollectionInitializer.getPrivatePropertyName(property)
                    if (propInitString.startsWith(privatePropertyName).not()) {
                        initializeFunction.addCode(propInitString)
                        initializeFunction.addCode("\n")
                    }
                }
            }

        targetFile.addFunction(initializeFunction.build())
    }

    companion object {
        const val INITIALIZE_NULL_PROPERTIES_FUNCTION_NAME = "initializeAllNullableProperties"
        private val valvarRegex = "^\\s*va[lr]\\s+".toRegex()
        private val valvarRegexWithVisibilityPrefix = "^.*\\s+va[lr]\\s+".toRegex()
        private val typeDefinitionRegex = ":.*=".toRegex()
    }

}
