package de.wiejack.kreator.builder.processor.propertyinitializer

import com.google.devtools.ksp.getClassDeclarationByName
import com.google.devtools.ksp.processing.Resolver
import com.google.devtools.ksp.symbol.KSPropertyDeclaration
import com.google.devtools.ksp.symbol.KSType
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.PropertySpec
import de.wiejack.kreator.builder.processor.api.BuildContext
import de.wiejack.kreator.builder.processor.api.PropertyInitializer
import de.wiejack.kreator.builder.processor.api.resolveCached
import java.math.BigDecimal
import java.sql.Timestamp
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.OffsetDateTime
import java.time.OffsetTime
import java.time.ZonedDateTime
import java.util.*
import kotlin.random.Random
import kotlin.reflect.KClass
import kotlin.reflect.full.createInstance

sealed class AbstractPropertyInitializer(
    private val kclass: KClass<*>,
    private val importContext: ImportContext,
    override val order: Int,
) : PropertyInitializer {
    companion object {
        fun allSimplePropertyInitializer(): List<AbstractPropertyInitializer> =
            AbstractPropertyInitializer::class
                .sealedSubclasses
                .map { clazz -> clazz.createInstance() }
                .toList()
    }

    open fun type(resolver: Resolver): KSType =
        resolver.getClassDeclarationByName(kclass.java.canonicalName)?.asType(emptyList())
            ?: error("Expecting resolvable type: ${kclass.java.canonicalName}")

    override fun couldInitialize(
        property: KSPropertyDeclaration,
        buildContext: BuildContext,
    ): Boolean =
        try {
            if (buildContext.initializeNullableProperties) {
                property.type.resolve().makeNotNullable() == type(buildContext.resolver)
            } else {
                property.type.resolveCached() == type(buildContext.resolver)
            }
        } catch (e: Exception) {
            false
        }

    override fun initializeProperty(
        propertyDeclaration: KSPropertyDeclaration,
        buildContext: BuildContext,
        targetFile: FileSpec.Builder,
        propertyConsumer: (PropertySpec) -> Unit,
    ) {
        propertyConsumer.invoke(
            PropertySpec
                .builder(propertyDeclaration.simpleName.asString(), kclass)
                .mutable(true)
                .initializer(importContext.initializer)
                .build(),
        )
        importContext.importClasses.forEach {
            targetFile.addImport(it, "")
        }
        importContext.importStrings.forEach {
            targetFile.addImport(it.substringBeforeLast("."), it.substringAfterLast("."))
        }
    }
}

data class ImportContext(
    val importClasses: Set<KClass<*>> = emptySet(),
    val importStrings: Set<String> = emptySet(),
    val initializer: String,
)

class StringPropertyInitializer :
    AbstractPropertyInitializer(
        kclass = String::class,
        importContext =
            ImportContext(
                importStrings = setOf("de.wiejack.kreator.builder.api.randomString"),
                initializer = "randomString(5)",
            ),
        order = 15000,
    ) {
    override fun type(resolver: Resolver): KSType = resolver.builtIns.stringType
}

class IntPropertyInitializer :
    AbstractPropertyInitializer(
        kclass = Int::class,
        importContext = ImportContext(importClasses = setOf(Random::class), initializer = "Random.nextInt()"),
        order = 16000,
    ) {
    override fun type(resolver: Resolver): KSType = resolver.builtIns.intType
}

class NumberPropertyInitializer :
    AbstractPropertyInitializer(
        kclass = Number::class,
        importContext = ImportContext(importClasses = setOf(Random::class), initializer = "Random.nextInt()"),
        order = 17000,
    ) {
    override fun type(resolver: Resolver): KSType = resolver.builtIns.numberType
}

class LongPropertyInitializer :
    AbstractPropertyInitializer(
        kclass = Long::class,
        importContext = ImportContext(importClasses = setOf(Random::class), initializer = "Random.nextLong()"),
        order = 18000,
    ) {
    override fun type(resolver: Resolver): KSType = resolver.builtIns.longType
}

class ShortPropertyInitializer :
    AbstractPropertyInitializer(
        kclass = Short::class,
        importContext = ImportContext(importClasses = setOf(Random::class), initializer = "Random.nextInt().toShort()"),
        order = 19000,
    ) {
    override fun type(resolver: Resolver): KSType = resolver.builtIns.shortType
}

class FloatPropertyInitializer :
    AbstractPropertyInitializer(
        kclass = Float::class,
        importContext = ImportContext(importClasses = setOf(Random::class), initializer = "Random.nextFloat()"),
        order = 20000,
    ) {
    override fun type(resolver: Resolver): KSType = resolver.builtIns.floatType
}

class DoublePropertyInitializer :
    AbstractPropertyInitializer(
        kclass = Double::class,
        importContext = ImportContext(importClasses = setOf(Random::class), initializer = "Random.nextDouble()"),
        order = 21000,
    ) {
    override fun type(resolver: Resolver): KSType = resolver.builtIns.doubleType
}

class BooleanPropertyInitializer :
    AbstractPropertyInitializer(
        kclass = Boolean::class,
        importContext = ImportContext(importClasses = setOf(Random::class), initializer = "Random.nextBoolean()"),
        order = 22000,
    ) {
    override fun type(resolver: Resolver): KSType = resolver.builtIns.booleanType
}

class CharPropertyInitializer :
    AbstractPropertyInitializer(
        kclass = Char::class,
        importContext =
            ImportContext(
                importStrings = setOf("de.wiejack.kreator.builder.api.randomChar"),
                initializer = "randomChar()",
            ),
        order = 23000,
    ) {
    override fun type(resolver: Resolver): KSType = resolver.builtIns.charType
}

class LocalDateTypePropertyInitializer :
    AbstractPropertyInitializer(
        kclass = LocalDate::class,
        importContext = ImportContext(importClasses = setOf(LocalDate::class), initializer = """LocalDate.now()"""),
        order = 24000,
    )

class LocalDateTimeTypePropertyInitializer :
    AbstractPropertyInitializer(
        kclass = LocalDateTime::class,
        importContext =
            ImportContext(
                importClasses = setOf(LocalDateTime::class),
                initializer = """LocalDateTime.now()""",
            ),
        order = 25000,
    )

class TimestampTypePropertyInitializer :
    AbstractPropertyInitializer(
        kclass = Timestamp::class,
        importContext =
            ImportContext(
                importClasses = setOf(Timestamp::class, Instant::class),
                initializer = """Timestamp.from(Instant.now())""",
            ),
        order = 26000,
    )

class InstantTypePropertyInitializer :
    AbstractPropertyInitializer(
        kclass = Instant::class,
        importContext = ImportContext(importClasses = setOf(Instant::class), initializer = """Instant.now()"""),
        order = 27000,
    )

class BigDecimalPropertyInitializer :
    AbstractPropertyInitializer(
        kclass = BigDecimal::class,
        importContext = ImportContext(importClasses = setOf(BigDecimal::class), initializer = """BigDecimal.ZERO"""),
        order = 28000,
    )

class UUIDPropertyInitializer :
    AbstractPropertyInitializer(
        kclass = UUID::class,
        importContext = ImportContext(importClasses = setOf(UUID::class), initializer = """UUID.randomUUID()"""),
        order = 29000,
    )

class OffsetDateTimePropertyInitializer :
    AbstractPropertyInitializer(
        kclass = OffsetDateTime::class,
        importContext =
            ImportContext(
                importClasses = setOf(OffsetDateTime::class),
                initializer = """OffsetDateTime.now()""",
            ),
        order = 30000,
    )

class OffsetTimePropertyInitializer :
    AbstractPropertyInitializer(
        kclass = OffsetTime::class,
        importContext = ImportContext(importClasses = setOf(OffsetTime::class), initializer = """OffsetTime.now()"""),
        order = 31000,
    )

class ZonedDateTimePropertyInitializer :
    AbstractPropertyInitializer(
        kclass = ZonedDateTime::class,
        importContext =
            ImportContext(
                importClasses = setOf(ZonedDateTime::class),
                initializer = """ZonedDateTime.now()""",
            ),
        order = 32000,
    )

class LocalTimePropertyInitializer :
    AbstractPropertyInitializer(
        kclass = LocalTime::class,
        importContext =
            ImportContext(
                importClasses = setOf(LocalTime::class),
                initializer = """LocalTime.now()""",
            ),
        order = 33000,
    )

class TimeZonePropertyInitializer :
    AbstractPropertyInitializer(
        kclass = TimeZone::class,
        importContext =
            ImportContext(
                importClasses = setOf(TimeZone::class),
                initializer = """TimeZone.getDefault()""",
            ),
        order = 34000,
    )

class CurrencyPropertyInitializer :
    AbstractPropertyInitializer(
        kclass = Currency::class,
        importContext =
            ImportContext(
                importClasses = setOf(Currency::class),
                initializer = """Currency.getInstance("EUR")""",
            ),
        order = 35000,
    )

class LocalePropertyInitializer :
    AbstractPropertyInitializer(
        kclass = Locale::class,
        importContext =
            ImportContext(
                importClasses = setOf(Locale::class),
                initializer = """Locale.getDefault()""",
            ),
        order = 36000,
    )

class BytePropertyInitializer :
    AbstractPropertyInitializer(
        kclass = Byte::class,
        importContext =
            ImportContext(
                importClasses = setOf(Random::class),
                initializer = """Random.nextBytes(1).first()""",
            ),
        order = 37000,
    ) {
    override fun type(resolver: Resolver): KSType = resolver.builtIns.byteType
}

class ByteArrayPropertyInitializer :
    AbstractPropertyInitializer(
        kclass = ByteArray::class,
        importContext =
            ImportContext(
                importClasses = emptySet(),
                initializer = """ByteArray(10)""",
            ),
        order = 38000,
    ) {
    override fun type(resolver: Resolver): KSType =
        resolver.getClassDeclarationByName<ByteArray>()?.asType(emptyList())
            ?: error("Expecting resolvable type: ${ByteArray::class.java.canonicalName}")
}

class LongArrayPropertyInitializer :
    AbstractPropertyInitializer(
        kclass = LongArray::class,
        importContext =
            ImportContext(
                importClasses = emptySet(),
                initializer = """LongArray(10)""",
            ),
        order = 39000,
    ) {
    override fun type(resolver: Resolver): KSType =
        resolver.getClassDeclarationByName<LongArray>()?.asType(emptyList())
            ?: error("Expecting resolvable type: ${LongArray::class.java.canonicalName}")
}

class FloatArrayPropertyInitializer :
    AbstractPropertyInitializer(
        kclass = FloatArray::class,
        importContext =
            ImportContext(
                importClasses = emptySet(),
                initializer = """FloatArray(10)""",
            ),
        order = 40000,
    ) {
    override fun type(resolver: Resolver): KSType =
        resolver.getClassDeclarationByName<FloatArray>()?.asType(emptyList())
            ?: error("Expecting resolvable type: ${FloatArray::class.java.canonicalName}")
}

class ShortArrayPropertyInitializer :
    AbstractPropertyInitializer(
        kclass = ShortArray::class,
        importContext =
            ImportContext(
                importClasses = emptySet(),
                initializer = """ShortArray(10)""",
            ),
        order = 41000,
    ) {
    override fun type(resolver: Resolver): KSType =
        resolver.getClassDeclarationByName<ShortArray>()?.asType(emptyList())
            ?: error("Expecting resolvable type: ${ShortArray::class.java.canonicalName}")
}

class IntArrayPropertyInitializer :
    AbstractPropertyInitializer(
        kclass = IntArray::class,
        importContext =
            ImportContext(
                importClasses = emptySet(),
                initializer = """IntArray(10)""",
            ),
        order = 42000,
    ) {
    override fun type(resolver: Resolver): KSType =
        resolver.getClassDeclarationByName<IntArray>()?.asType(emptyList())
            ?: error("Expecting resolvable type: ${IntArray::class.java.canonicalName}")
}

class DoubleArrayPropertyInitializer :
    AbstractPropertyInitializer(
        kclass = DoubleArray::class,
        importContext =
            ImportContext(
                importClasses = emptySet(),
                initializer = """DoubleArray(10)""",
            ),
        order = 43000,
    ) {
    override fun type(resolver: Resolver): KSType =
        resolver.getClassDeclarationByName<DoubleArray>()?.asType(emptyList())
            ?: error("Expecting resolvable type: ${DoubleArray::class.java.canonicalName}")
}
