package de.wiejack.kreator.builder.processor

import com.google.devtools.ksp.processing.KSPLogger
import com.google.devtools.ksp.processing.Resolver
import com.google.devtools.ksp.symbol.KSClassDeclaration
import de.wiejack.kreator.builder.processor.api.BuildContext
import de.wiejack.kreator.builder.processor.api.BuilderClassName
import de.wiejack.kreator.builder.processor.api.CommonKspContext
import de.wiejack.kreator.builder.processor.api.PackageName
import de.wiejack.kreator.builder.processor.api.SourceClassName

class CommonKSPContextImpl(
    override val resolver: Resolver,
    override val logger: KSPLogger,
) : CommonKspContext

data class BuildContextImpl(
    override val sourceProperty: KSClassDeclaration,
    override val initializeNullableProperties: Boolean,
    override val initializeCollectionsCount: Int,
    override val useCompanionObject: Boolean,
    override val resolver: Resolver,
    override val logger: KSPLogger,
) : BuildContext {
    val sourceClassName = SourceClassName(sourceProperty.simpleName.asString())
    override val builderClassName = builderClassNameFor(sourceProperty)
    override val packageName = PackageName(sourceProperty.packageName.asString())

    override fun builderClassNameFor(classDeclaration: KSClassDeclaration): BuilderClassName =
        BuilderClassName(classDeclaration.simpleName.getShortName() + "Builder")
}
