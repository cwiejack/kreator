package de.wiejack.kreator.builder.processor.creator

import com.google.devtools.ksp.symbol.KSClassDeclaration
import com.squareup.kotlinpoet.AnnotationSpec
import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.ParameterSpec
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import com.squareup.kotlinpoet.ksp.toTypeName
import de.wiejack.kreator.builder.processor.api.BuildContext

class CollectionDslCreator {
    internal fun createExtensionFunction(
        targetFile: FileSpec.Builder,
        classDeclaration: KSClassDeclaration,
        buildContext: BuildContext,
    ) {
        val type = classDeclaration.asType(emptyList())
        val packageName = buildContext.packageName.name

        targetFile.addImport(packageName, "builder")
        val withRandomValuesFunction =
            FunSpec
                .builder("withRandomValues")
                .receiver(ClassName("de.wiejack.kreator.builder.api", "CollectionDsl").parameterizedBy(type.toTypeName()))
                .addParameter(ParameterSpec.builder("count", Int::class).defaultValue("2").build())
                .addAnnotation(
                    AnnotationSpec
                        .builder(JvmName::class)
                        .addMember("\"withRandomValues${type.declaration.simpleName.asString()}\"")
                        .build(),
                ).addCode("""target.addAll((1..count).map { ${buildContext.builderReference()}})""")

        targetFile.addFunction(withRandomValuesFunction.build())
    }
}
