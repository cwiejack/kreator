package de.wiejack.kreator.builder.processor.propertyinitializer

import com.google.devtools.ksp.KspExperimental
import com.google.devtools.ksp.getAnnotationsByType
import com.google.devtools.ksp.getClassDeclarationByName
import com.google.devtools.ksp.isAbstract
import com.google.devtools.ksp.processing.Resolver
import com.google.devtools.ksp.symbol.KSClassDeclaration
import com.google.devtools.ksp.symbol.KSFile
import com.google.devtools.ksp.symbol.KSPropertyDeclaration
import com.google.devtools.ksp.symbol.KSTypeReference
import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ksp.toTypeName
import de.wiejack.kreator.builder.api.Builder
import de.wiejack.kreator.builder.processor.api.BuildContext
import de.wiejack.kreator.builder.processor.api.CommonKspContext
import de.wiejack.kreator.builder.processor.api.PropertyInitializer
import de.wiejack.kreator.builder.processor.api.resolveCached
import de.wiejack.kreator.builder.processor.propertyClassDeclaration

@OptIn(KspExperimental::class)
class OtherBuilderAnnotatedPropertyInitializer : PropertyInitializer {

    companion object {
        fun isBuilderAnnotatedType(type: KSTypeReference, commonKspContext: CommonKspContext): Boolean {
            val resolver = commonKspContext.resolver

            val className: ClassName? = when (val typeName = type.resolveCached().toTypeName()) {
                is ClassName -> ClassName.bestGuess(typeName.canonicalName)
                is Dynamic -> null
                is LambdaTypeName -> null
                is ParameterizedTypeName -> typeName.rawType.topLevelClassName()
                is TypeVariableName -> null
                is WildcardTypeName -> typeName.outTypes.firstOrNull()?.let { it as? ClassName }?.topLevelClassName()
            }
            if (className == null) {
                return false
            }
            val classDeclarationByName = resolver.getClassDeclarationByName(className.canonicalName)

            val derivedClassDeclarations: List<KSClassDeclaration> = if (classDeclarationByName?.isAbstract() == true) {
                findAssignableFromAbstractType(resolver, type).toList()
            } else emptyList()

            val sealedClassDeclarations = classDeclarationByName?.getSealedSubclasses()?.toList() ?: emptyList()

            return sealedClassDeclarations
                .plus(derivedClassDeclarations)
                .plus(classDeclarationByName)
                .any { it.hasBuilderAnnotation() }
        }

        private fun findAssignableFromAbstractType(
            resolver: Resolver,
            type: KSTypeReference
        ) = resolver.getAllFiles()
            .flatMap(KSFile::declarations)
            .filterIsInstance<KSClassDeclaration>()
            .filter { ksClassDeclaration ->
                ksClassDeclaration.isAbstract().not() and
                        type.resolveCached().isAssignableFrom(ksClassDeclaration.asType(emptyList()))
            }

        private fun KSClassDeclaration?.hasBuilderAnnotation(): Boolean {
            return (this?.getAnnotationsByType(Builder::class)?.count() ?: 0) > 0
        }

    }

    override fun couldInitialize(
        property: KSPropertyDeclaration,
        buildContext: BuildContext,
    ): Boolean {
        return isBuilderAnnotatedType(property.type, buildContext)
    }

    override fun initializeProperty(
        propertyDeclaration: KSPropertyDeclaration,
        buildContext: BuildContext,
        targetFile: FileSpec.Builder,
        propertyConsumer: (PropertySpec) -> Unit,
    ) {
        val classDeclaration: KSClassDeclaration =
            determineConcreteBuilderClassDeclaration(propertyDeclaration, buildContext)
        val builderClassName = buildContext.builderClassNameFor(classDeclaration)

        val propertyType =
            if (propertyDeclaration.type.resolve().isMarkedNullable) {
                PropertySpec.builder(
                    name = propertyDeclaration.simpleName.asString(),
                    type = LambdaTypeName.get(
                        receiver = ClassName(classDeclaration.packageName.asString(), builderClassName.name),
                        returnType = ClassName("kotlin", listOf("Unit"))
                    ).copy(nullable = true)
                )
                    .mutable(true)
                    .initializer(if (buildContext.initializeNullableProperties) "{}" else "null")
            } else {
                PropertySpec.builder(
                    propertyDeclaration.simpleName.asString(),
                    ClassName(classDeclaration.packageName.asString(), builderClassName.name)
                )
                    .mutable(true)
                    .initializer("${builderClassName.name}()")
            }

        propertyConsumer.invoke(propertyType.build())
    }

    override val order: Int
        get() = 14000

    private fun determineConcreteBuilderClassDeclaration(
        propertyDeclaration: KSPropertyDeclaration,
        buildContext: BuildContext
    ): KSClassDeclaration {
        val classDeclaration = propertyClassDeclaration(propertyDeclaration)
        val sealedSubDeclarations = classDeclaration.getSealedSubclasses().toList()
        return sealedSubDeclarations
            .plus(findAssignableFromAbstractType(buildContext.resolver, propertyDeclaration.type))
            .plus(classDeclaration)
            .first { it.hasBuilderAnnotation() }
    }
}
