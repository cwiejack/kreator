package de.wiejack.kreator.kopier.processor

import com.google.devtools.ksp.processing.CodeGenerator
import com.google.devtools.ksp.processing.KSPLogger
import com.google.devtools.ksp.processing.Resolver
import com.google.devtools.ksp.processing.SymbolProcessor
import com.google.devtools.ksp.symbol.KSAnnotated
import com.google.devtools.ksp.symbol.KSClassDeclaration
import com.google.devtools.ksp.symbol.KSFile
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.ksp.kspDependencies
import com.squareup.kotlinpoet.ksp.originatingKSFiles
import de.wiejack.kreator.kopier.api.Copy
import de.wiejack.kreator.kopier.processor.creator.KopierClassCreator
import de.wiejack.kreator.kopier.processor.creator.KopierExtensionFunctionCreator
import java.io.OutputStreamWriter
import java.nio.charset.StandardCharsets

class KopierSymbolProcessor(
    private val codeGenerator: CodeGenerator,
    private val logger: KSPLogger,
) : SymbolProcessor {

    private val kopierClassCreator = KopierClassCreator()
    private val kopierExtensionFunctionCreator = KopierExtensionFunctionCreator()
    override fun process(resolver: Resolver): List<KSAnnotated> {
        logger.info("Starting Kreator Kopier Processor...")
        resolveAllKopierDeclarations(resolver).forEach {
            createKopierFile(it)
        }
        return emptyList()
    }

    private fun createKopierFile(
        kopierClassDeclaration: KSClassDeclaration,
    ) {
        val kopierPackage = kopierClassDeclaration.packageName.asString()
        val kopierFileName = kopierClassDeclaration.simpleName.getShortName() + "Kopier"

        val kopierContext = KopierContextImpl(kopierClassDeclaration)
        FileSpec.builder(kopierPackage, kopierFileName)
            .also { fileBuilder ->
                kopierClassCreator.create(fileBuilder, kopierContext)
                kopierExtensionFunctionCreator.create(fileBuilder, kopierContext)
            }
            .build()
            .write(codeGenerator, false, listOfNotNull(kopierClassDeclaration.containingFile))
    }


    private fun resolveAllKopierDeclarations(resolver: Resolver) = resolver
        .getSymbolsWithAnnotation(Copy::class.java.canonicalName)
        .filterIsInstance<KSClassDeclaration>()

    private fun FileSpec.write(
        codeGenerator: CodeGenerator,
        aggregating: Boolean,
        originatingKSFiles: Iterable<KSFile> = originatingKSFiles()
    ) {
        val dependencies = kspDependencies(aggregating, originatingKSFiles)
        val file =
            codeGenerator.createNewFileByPath(dependencies, "kreator/kopier/${packageName.replace(".", "/")}/$name")
        // Don't use writeTo(file) because that tries to handle directories under the hood
        OutputStreamWriter(file, StandardCharsets.UTF_8)
            .use(::writeTo)
    }
}
