package de.wiejack.kreator.kopier.processor

import com.google.devtools.ksp.KspExperimental
import com.google.devtools.ksp.isAnnotationPresent
import com.google.devtools.ksp.isPrivate
import com.google.devtools.ksp.processing.Resolver
import com.google.devtools.ksp.symbol.KSClassDeclaration
import de.wiejack.kreator.builder.api.NoBuilder
import de.wiejack.kreator.builder.processor.api.BuilderClassName
import de.wiejack.kreator.builder.processor.api.CommonKspContext
import de.wiejack.kreator.builder.processor.api.PackageName
import de.wiejack.kreator.kopier.api.NoCopy


@JvmInline
value class KopierClassName(val name: String)

@JvmInline
value class SourceClassName(val name: String)

@JvmInline
value class PackageName(val name: String)


@JvmInline
value class KopierFunctionName(val name: String)

interface KopierContext {
    val sourceClass: KSClassDeclaration
    val packageName: PackageName
    val kopierClassName: KopierClassName
    val kopierFunctionName: KopierFunctionName
    @OptIn(KspExperimental::class)
    fun buildableProperties() = sourceClass.getAllProperties()
        .filter { it.isAnnotationPresent(NoCopy::class).not() }
        .filter { it.isPrivate().not() }
        .toList()
}
class KopierContextImpl(override val sourceClass: KSClassDeclaration): KopierContext {
    override val packageName: PackageName
        get() = PackageName(sourceClass.packageName.asString())
    override val kopierClassName: KopierClassName
        get() = KopierClassName(sourceClass.simpleName.getShortName() + "Kopier")
    override val kopierFunctionName: KopierFunctionName
        get() = KopierFunctionName("copy")
}
