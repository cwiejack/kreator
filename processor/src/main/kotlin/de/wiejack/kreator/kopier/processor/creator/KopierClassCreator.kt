package de.wiejack.kreator.kopier.processor.creator

import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.PropertySpec
import com.squareup.kotlinpoet.TypeSpec
import com.squareup.kotlinpoet.ksp.toTypeName
import de.wiejack.kreator.builder.processor.api.resolveCached
import de.wiejack.kreator.kopier.api.KopierDslMarker
import de.wiejack.kreator.kopier.processor.KopierContext

class KopierClassCreator {
    fun create(targetFile: FileSpec.Builder, context: KopierContext) {
        val kopierClass = TypeSpec.classBuilder(context.kopierClassName.name).apply {
            addAnnotation(KopierDslMarker::class)
            primaryConstructor(
                FunSpec.constructorBuilder()
                    .addParameter("orig", context.sourceClass.asType(emptyList()).toTypeName())
                    .build()
            )
            context.buildableProperties().forEach { property ->
                val prop = PropertySpec.builder(
                    property.simpleName.asString(),
                    property.type.resolveCached().toTypeName()
                )
                    .mutable(true)
                    .initializer("orig.${property.simpleName.asString()}")
                    .build()
                addProperty(prop)
            }
        }

        targetFile.addType(kopierClass.build())
    }
}
