package de.wiejack.kreator.kopier.processor

import com.google.auto.service.AutoService
import com.google.devtools.ksp.processing.SymbolProcessor
import com.google.devtools.ksp.processing.SymbolProcessorEnvironment
import com.google.devtools.ksp.processing.SymbolProcessorProvider
import de.wiejack.kreator.builder.processor.BuilderSymbolProcessor

@AutoService(SymbolProcessorProvider::class)
class KopierSymbolProcessorProvider : SymbolProcessorProvider {
    override fun create(environment: SymbolProcessorEnvironment): SymbolProcessor {
        return KopierSymbolProcessor(environment.codeGenerator, environment.logger)
    }

}
