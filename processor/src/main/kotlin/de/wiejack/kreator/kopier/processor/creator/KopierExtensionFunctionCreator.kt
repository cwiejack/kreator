package de.wiejack.kreator.kopier.processor.creator

import com.google.devtools.ksp.KspExperimental
import com.google.devtools.ksp.getConstructors
import com.google.devtools.ksp.isAnnotationPresent
import com.google.devtools.ksp.symbol.KSPropertyDeclaration
import com.google.devtools.ksp.symbol.Modifier
import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.LambdaTypeName
import com.squareup.kotlinpoet.ParameterSpec
import com.squareup.kotlinpoet.ksp.toClassName
import de.wiejack.kreator.kopier.api.CopyConstructor
import de.wiejack.kreator.kopier.processor.KopierContext

class KopierExtensionFunctionCreator {

    fun create(targetFile: FileSpec.Builder, context: KopierContext) {
        val packageName = context.packageName.name
        val fqnClassName = context.sourceClass.toClassName().canonicalName
        val kopierClassName = ClassName(packageName, context.kopierClassName.name)

        val extensionFunction = FunSpec.builder(context.kopierFunctionName.name)
            .receiver(ClassName(packageName, listOf(fqnClassName)))
            .returns(ClassName(packageName, listOf(fqnClassName)))
            .addParameter(
                ParameterSpec.builder(
                    "block", LambdaTypeName.get(
                        receiver = kopierClassName,
                        returnType = ClassName("kotlin", listOf("Unit"))
                    )
                ).build()
            )
            .addStatement("val kopier = ${kopierClassName.canonicalName}(this)")
            .addStatement("block.invoke(kopier)")
            .addStatement("""return ${instanceCreationString(targetFile, context)}""".trimIndent())

        targetFile.addFunction(extensionFunction.build())
    }

    private fun instanceCreationString(targetFile: FileSpec.Builder, context: KopierContext): String {
        val fqnClassName = context.sourceClass.toClassName().canonicalName

        val constructorParams = findConstructor(context).parameters

        val (constructorProperties, remainingProperties) = context.buildableProperties()
            .partition { prop -> constructorParams.any { cProp -> cProp.name?.asString() == prop.simpleName.asString() } }
        val (withSetterProperties, noSetterProperties) = remainingProperties.partition(::hasVisibleSetter)


        val instanceCreationString = """
            $fqnClassName(
                ${initializeConstructorProperties(constructorProperties)}
            )
            ${initializeSetterProperties(withSetterProperties)}
            ${initializeWithReflection(noSetterProperties, targetFile, context)}
        """.trimIndent()

        return instanceCreationString
    }

    private fun initializeConstructorProperties(
        constructorProperties: List<KSPropertyDeclaration>,
    ): String {
        return constructorProperties.joinToString(",\n") { property ->
            "${property.simpleName.asString()} = kopier.${property.simpleName.asString()}"
        }
    }

    private fun initializeSetterProperties(
        setterProperties: List<KSPropertyDeclaration>,
    ): String {
        return if (setterProperties.isNotEmpty()) {
            val setProperties = setterProperties.joinToString("\n") { property ->
                "${property.simpleName.asString()} = kopier.${property.simpleName.asString()}"
            }
            """.apply {
                $setProperties
            }
            """.trimIndent()
        } else {
            ""
        }
    }

    private fun initializeWithReflection(
        noSetterProperties: List<KSPropertyDeclaration>,
        fileType: FileSpec.Builder,
        context: KopierContext
    ): String {
        return if (noSetterProperties.isNotEmpty()) {
            fileType.addImport("de.wiejack.kreator", "findField")

            val reflectionValues = noSetterProperties.joinToString("\n") { property ->
                """
                    findField(${context.sourceClass.simpleName.asString()}::class.java, "${property.simpleName.asString()}")?.run{
                        this.isAccessible = true
                        set(this@apply, kopier.${property.simpleName.asString()})
                    }
                """.trimIndent()
            }
            """.apply {
                    $reflectionValues
                }
            """.trimIndent()
        } else {
            ""
        }
    }

    @OptIn(KspExperimental::class)
    private fun findConstructor(context: KopierContext) =
        (context.sourceClass.getConstructors().firstOrNull { it.isAnnotationPresent(CopyConstructor::class) }
            ?: context.sourceClass.primaryConstructor
            ?: throw IllegalStateException("${context.sourceClass.qualifiedName?.asString()} must have either a @CopyConstructor annotated constructor or a primary constructor."))

    private fun hasVisibleSetter(property: KSPropertyDeclaration) =
        (property.setter != null) and ((Modifier.PUBLIC in (property.setter?.modifiers
            ?: emptySet())) or (Modifier.INTERNAL in (property.setter?.modifiers ?: emptySet())))

}
