# kreator

kreator is a library to automatically create useful helper functions for ```Kotlin``` developers.
Currently available:

- builder (creates builder for your classes)
- kopier (creates a copy function for your instances)

## Builder

### Intention

An often used pattern for testing is to create instances of classes specially configured for a test case. And most of
the time these instances must have
some special configured properties but not all properties of the instance matter. In Kotlin these properties must be
initialized if they are not marked
as nullable and this is often noisy code that distracts from the important properties. This is where the kreator builder
can help you.

### Limiting factors

The builder should be used for ```data classes``` and normal ```pojo classes```.
The builder can be generated as a normal function or as an extension function for the ```companion object``` of the
class to build.
The builder function or the extension function gets created by ```ksp``` at build time.

### Features

Kreator generates a builder function which will automatically generates default values for all non-nullable properties
of the target instance. If the builder does not know how to create a
default value you can tell the builder how to create a value with the help of an annotation or register a custom
initializer via ServiceLoader mechanism. In addition, the builder contains a special DSL for collections.

### Installation

To use ``kreator`` you must add 2 dependencies. One jar for the ``api`` and one for the ``processor`` and additionally
the ``ksp`` plugin.

You find the latest version on Maven Central: https://central.sonatype.com/search?namespace=de.wiejack.kreator
Example build.gradle.kts:

```
plugins {
    ...
    id("com.google.devtools.ksp") version "<appropriate version for your kotlin version>"
}

dependencies {
    ...
    ksp("de.wiejack.kreator:processor:<version>")
    implementation("de.wiejack.kreator:api:<version>")
    ...
}
```

If you only want to use ``kreator`` on test classes you should use

```
dependencies {
    ...
    kspTest("de.wiejack.kreator:processor:<version>")
    testImplementation("de.wiejack.kreator:api:<version>")
    ...
}
```

### Usage

#### 1.1 Basic Example with extension function (default)

Given an example class like this:

```kotlin
import de.wiejack.kreator.builder.api.Builder

@Builder
class SimpleTypeExample(
    val aString: String,
    val aNullableString: String?
) {
    var anInt: Int = 0
    var aNullableInt: Int? = null

    companion object {}
}
```

Kreator generates a ```builder``` Extension so that you get a ready to use instance like this:

```kotlin
val example: SimpleTypeExample = SimpleTypeExample.builder { }
```

If you want to customize any of the existing properties you can do it like so:

```kotlin
val example: SimpleTypeExample = SimpleTypeExample.builder {
    aString = "the right text in here"
    aNullableString = "or here"
    anInt = 42
    aNullableInt = 42
}
```

#### 1.2 Basic Example with root builder function

Given an example class like this:

```kotlin
import de.wiejack.kreator.builder.api.Builder

@Builder(useCompanionObject = false)
class SimpleTypeExample(
    val aString: String,
    val aNullableString: String?
) {
    var anInt: Int = 0
    var aNullableInt: Int? = null

    companion object {}
}
```

Kreator generates a function called ```SimpleTypeExampleBuilder``` Extension so that you get a ready to use instance
like this:

```kotlin
val example: SimpleTypeExample = SimpleTypeExampleBuilder { }
```

If you want to customize any of the existing properties you can do it like so:

```kotlin
val example: SimpleTypeExample = SimpleTypeExampleBuilder {
    aString = "the right text in here"
    aNullableString = "or here"
    anInt = 42
    aNullableInt = 42
}
```

##### 1.3. Null handling

The default for nullable properties is an initialization with ```null```.
If you want to change this behavior you can configure the builder to initialize nullable properties.

```kotlin
@Builder(initializeNullableProperties = true)
class SimpleTypeExample(
    val aString: String,
    val aNullableString: String?
)
```

With this configuration the builder will initialize the nullable properties with default values.
Alternatively you can use an automatically generated extension function (which is available for all builder functions
regardless of the configuration at annotation level)
to initialize nullable properties with random values.

```kotlin
val example: SimpleTypeExample = SimpleTypeExample.builder {
    initializeAllNullableProperties()
}
```

#### 2. Collection Example

For collection types (currently Set, List and Map) Kreator generates the builder with a special ```CollectionDsl``` for
Sets and Lists, and ```MapDsl``` for Maps.
This DSL should simplify the handling of collections.

Given the following example:

```kotlin
import de.wiejack.kreator.builder.api.Builder

@Builder
class CollectionExample(
    val stringList: List<String>,
    val intSet: Set<Int>,
    val nullableList: List<Boolean>?,
    val aMap: Map<String, Int>,
) {

    companion object {}
}
```

Kreator generates a ```builder``` Extension so that you get a ready to use instance like this:

```kotlin
 val example = CollectionExample.builder {}
```

If you want to customize the properties you can do it like so:

```kotlin
 val example = CollectionExample.builder {
    stringList {
        +"a string"
        this add "another string"
        withRandomValues(4)
    }
    intSet {
        this add 42
        withRandomValues(3)
    }
    
    aMap {
        this add ("key" to 42)
        +("anotherKey" to 43)
    }
}
```

As you can see for each non-nullable collection property the builder has a custom dsl initialization property.
The ```+``` operator can be used where Kotlin has no default operator function in place. This is the reason why there is
no
```+42``` example.
For every simple type like (string, int, double, etc.) Kreator comes with a custom extension for the collection dsl
called ```withRandomValues()```. For Maps this is not possible at the moment.
This function generates as much as specified (default 2) random values for your collection.
For every ```@Builder``` annotated class such an extension will be automatically generated.

##### 2.1. Create Prefilled Collections

You can also create prefilled collections (Sets, Lists). You can either configure that all collections for a class should be prefilled
with random values or
configure it for a specific collection property. Given the following example:

```kotlin
@Builder(initializeCollectionsCount = 3)
class PrefilledCollectionExample(
    val uuidList: List<UUID>,
    val localDateSet: Set<LocalDate>,
    @InitializeCollectionCount(4)
    val stringList: List<String>,
    val nullableList: List<Boolean>?
) {

    companion object
}
```

Kreator generates a ```builder``` Extension so that you get a ready to use instance like this:

```kotlin
 val example = PrefilledCollectionExample.builder {}
```

The generated instance will have 3 UUIDs in the uuidList, 3 LocalDate in the localDateSet, 4 Strings in the stringList
and the nullableList is null.

#### 3. Other custom classes

Kreator does not only work for simple types. If your class has properties of custom classes it will generate a builder
for you if the custom class also has an ```@Builder``` annotation
or you provide information how to initialize the property with the help of ```@PropertyInitializer``` (usage see below).
Given the following example:

```kotlin
import de.wiejack.kreator.builder.api.Builder

@Builder
class OtherBuilderExample(
    val buildable: BuildableExample,
    val nullableBuildable: BuildableExample?
) {
    companion object {}
}

@Builder
class BuildableExample(val aString: String) {
    companion object {}
}
```

Kreator generates a ```builder``` Extension so that you get a ready to use instance like this:

```kotlin
 val example = OtherBuilderExample.builder {}
```

If you want to customize the properties you can do it like so:

```kotlin
   val example = OtherBuilderExample.builder {
    buildable {
        aString = "a string"
    }
    nullableBuildable = {
        aString = "another string"
    }
}
```

#### 4. Custom Property initialization with annotation

If Kreator could not initialize the property of you class automatically or the chosen solution does not fit your needs,
you can
override the generation for every property with the annotation ```@PropertyInitializer```.

Given the following example:

```kotlin
import de.wiejack.kreator.builder.api.Builder

@Builder
class PropertyInitializerExample(
    @PropertyInitializer(initializerString = "The answer is 42.")
    val aString: String,
    @PropertyInitializer(initializerValue = "42")
    val anInt: Int,
    @PropertyInitializer(
        initializerValue = "PropertyInitializeImport.magicNumber()",
        imports = ["<full qualified package>.PropertyInitializeImport"]
    )
    val magicNumber: Int

) {
    companion object
}

class PropertyInitializeImport {

    companion object {
        fun magicNumber() = 42
    }
}
```

Kreator generates a ```builder``` Extension so that you get a ready to use instance like this:

```kotlin
      val example = PropertyInitializerExample.builder { }
```

#### 5. Special constructor usage

You can tell Kreator which constructor the builder should use. Given the following example:

```kotlin
import de.wiejack.kreator.builder.api.Builder

@Builder
class ConstructorExample(val one: Long) {

    lateinit var two: String

    @NoBuilder
    var usedSecondConstructor: Boolean? = null

    @BuilderConstructor
    constructor(one: Long, two: String) : this(one) {
        this.two = two
        usedSecondConstructor = true
    }

    companion object {}
}
```

Kreator generates a ```builder``` Extension so that you get a ready to use instance like this:

```kotlin
    val example = ConstructorExample.builder {}
```

As you can see in this example, you can also tell Kreator to ignore a property by annotating it with ```NoBuilder```.

#### 6. Extending the builder or collection dsl

You can provide extensions to the builder and collection dsl to create your own custom helper functions.
For example, given this class

```kotlin
import de.wiejack.kreator.builder.api.Builder

@Builder
class Movie(val name: String, type: String)
```

you could provide an extension for the builder:

```kotlin
fun MovieBuilder.action() {
    this.name = "Terminator 2"
    this.type = "Action"
}
fun MovieBuilder.romantic() {
    this.name = "Pretty Woman"
    this.type = "Romance"
}
```

With those extensions you can use the builder like so:

```kotlin
val actionMovie = Movie.builder {
    action()
}
val romanticMovie = Movie.builder {
    romantic()
}
```

With the same pattern you can also extend the Kreator collection dsl.

Given the following example:

```kotlin
import de.wiejack.kreator.builder.api.Builder

@Builder
class MovieCollection(val movies: List<Movie>)
```

and the following extension

```kotlin
fun CollectionDsl<Movie>.actionMovies(count: Int = 2) {
    target.addAll((1..count).map {
        Movie.builder {
            action()
        }
    })
}
```

you can use the builder like so:

```kotlin
    val movieCollection = MovieCollection.builder {
    movies {
        actionMovies(5)
    }
}
```

#### 7. Extending the annotation processor with custom property initializer implementations

To create the builder, kreator must know how to initialize each kind of type (i.e. string, int, double etc.). For each
type kreator could create automatically
there is an implementation of

```kotlin
de.wiejack.kreator.builder.processor.api.PropertyInitializer
```

build in. If you use a type kreator does not know about you could annotate the property with ```@PropertyInitializer```
as described in 4.
This is fine if you use this type rarely. If you do not want to use the annotation all over the place you could extend
the annotation processor
with your custom written ```de.wiejack.kreator.builder.processor.api.PropertyInitializer```.

##### create an implementation of  ```de.wiejack.kreator.builder.processor.api.PropertyInitializer```

i.e.

```kotlin
class MyCustomPropertyInitializer : PropertyInitializer {
    override fun couldInitialize(
        property: com.google.devtools.ksp.symbol.KSPropertyDeclaration,
        commonKspContext: CommonKspContext
    ): Boolean {
        val resolver = commonKspContext.resolver

        val customClassType: KSClassDeclaration =
            resolver.getClassDeclarationByName(CustomClass::class.qualifiedName!!)!!

        return property.type.resolveCached().isAssignableFrom(customClassType.asType(emptyList()))
    }

    override fun initializeProperty(
        propertyDeclaration: com.google.devtools.ksp.symbol.KSPropertyDeclaration,
        buildContext: BuildContext,
        targetFile: com.squareup.kotlinpoet.FileSpec.Builder,
        targetType: com.squareup.kotlinpoet.TypeSpec.Builder
    ) {
        targetType.addProperty(
            PropertySpec.builder(propertyDeclaration.simpleName.asString(), CustomClass::class.java)
                .mutable(true)
                .initializer("CustomClass(aCustomString = \"testMe\")")
                .build()
        )
        targetFile.addImport(CustomClass::class.java, "")
    }
}
```

kreator uses ```ksp``` and ```kotlinpoet```, so you must add the following dependencies to your project

```
compileOnly("com.google.devtools.ksp:symbol-processing-api:<version used by kreator>")
compileOnly("com.squareup:kotlinpoet-ksp:<version used by kreator>")
```

##### create ServiceLoader file

Kreator uses the Java ServiceLoader mechanism to locate all custom initializer. So you must create the following file

```
META-INF/services/de.wiejack.kreator.builder.processor.api.PropertyInitializer
```

and add the full qualified name of your implementation in it. If you have multiple implementations seperate with ```,```

#####

To let kreator pick up the implementations, the ```ksp``` build process must know about it. So you must add the
```jar``` with your initializer implementation and
ServiceLoader config file to the build file of your project. Put this to your gradle file.

```
...
ksp("de.wiejack.kreator:processor:<version>")
ksp(<reference to your jar>)
...
```

## Kopier

Kopier is a library to automatically create a copy function for your classes.

### Installation

To use ``kopier`` you must add 2 dependencies. One jar for the ``api`` and one for the ``processor`` and additionally
the ``ksp`` plugin.

You find the latest version on Maven Central: https://central.sonatype.com/search?namespace=de.wiejack.kreator
Example build.gradle.kts:

```
plugins {
    ...
    id("com.google.devtools.ksp") version "1.9.22-1.0.16"
}

dependencies {
    ...
    ksp("de.wiejack.kreator:processor:<version>")
    implementation("de.wiejack.kreator:api:<version>")
    ...
}
```

If you only want to use ``kreator`` on test classes you should use

```
dependencies {
    ...
    kspTest("de.wiejack.kreator:processor:<version>")
    testImplementation("de.wiejack.kreator:api:<version>")
    ...
}
```

### Usage

#### 1. Basic Example

To use Kopier you must annotate your class with ```@Copy```. Given the following example:

```kotlin
@Copy
@Builder
class SimpleCopy(val aString: String, val anInt: Int, val aBoolean: Boolean?) {
    var aDouble: Double? = null
}
```

Kopier generates a ```copy``` for your instances. So that you get a ready to use copy function like this:

```kotlin
val copy = SimpleCopy.builder {}.copy {
    aString = "a string"
    anInt = 42
    aBoolean = true
    aDouble = 42.0
}
```

## Support

Not yet written.

## Roadmap

Not yet written.

## Contributing

Not yet written.

## Authors and acknowledgment

Not yet written.

## Project status

Work in progress ;)
