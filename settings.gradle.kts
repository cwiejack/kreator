rootProject.name = "kreator"

pluginManagement {
    repositories {
        mavenLocal()
        mavenCentral()
        gradlePluginPortal()
    }
}
include("api")
include("processor")
include("test")
include("custompropertyinitializer")
