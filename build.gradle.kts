import org.jetbrains.kotlin.gradle.dsl.KotlinBaseExtension

plugins {
    kotlin("jvm") version "2.1.10" apply false
    id("com.google.devtools.ksp") version "2.1.10-1.0.30" apply false
    id("io.github.gradle-nexus.publish-plugin") version "2.0.0"
}

subprojects {
    configurations.all {
        with(resolutionStrategy) {
            cacheChangingModulesFor(0, TimeUnit.SECONDS)
            cacheDynamicVersionsFor(0, TimeUnit.SECONDS)
        }
    }

    extensions.findByType(JavaPluginExtension::class)?.apply {
        toolchain {
            languageVersion.set(JavaLanguageVersion.of(17))
            vendor.set(JvmVendorSpec.ADOPTIUM)
        }
    }

    extensions.findByType(KotlinBaseExtension::class)?.apply {
        jvmToolchain(17)
    }

    repositories {
        mavenLocal()
        mavenCentral()
    }
}

nexusPublishing {
    repositories {
        sonatype {
            nexusUrl.set(uri("https://s01.oss.sonatype.org/service/local/"))
            snapshotRepositoryUrl.set(uri("https://s01.oss.sonatype.org/content/repositories/snapshots/"))
        }
    }
}
