package de.wiejack.custom

import com.google.devtools.ksp.getClassDeclarationByName
import com.google.devtools.ksp.symbol.KSClassDeclaration
import com.squareup.kotlinpoet.PropertySpec
import de.wiejack.kreator.builder.processor.api.BuildContext
import de.wiejack.kreator.builder.processor.api.PropertyInitializer
import de.wiejack.kreator.builder.processor.api.resolveCached

class MyCustomPropertyInitializer : PropertyInitializer {
    override fun couldInitialize(
        property: com.google.devtools.ksp.symbol.KSPropertyDeclaration,
        buildContext: BuildContext,
    ): Boolean {
        val resolver = buildContext.resolver

        val customClassType: KSClassDeclaration =
            resolver.getClassDeclarationByName(CustomClass::class.qualifiedName!!)!!

        return property.type.resolveCached().isAssignableFrom(customClassType.asType(emptyList()))
    }

    override fun initializeProperty(
        propertyDeclaration: com.google.devtools.ksp.symbol.KSPropertyDeclaration,
        buildContext: BuildContext,
        targetFile: com.squareup.kotlinpoet.FileSpec.Builder,
        propertyConsumer: (PropertySpec) -> Unit,
    ) {
        propertyConsumer.invoke(
            PropertySpec.builder(propertyDeclaration.simpleName.asString(), CustomClass::class.java)
                .mutable(true)
                .initializer("CustomClass(aCustomString = \"testMe\")")
                .build()
        )
        targetFile.addImport(CustomClass::class.java, "")
    }
}
