import org.gradle.api.tasks.testing.logging.TestLogEvent

plugins {
    jacoco
    kotlin("jvm")
    id("com.google.devtools.ksp")
}


dependencies {
    implementation(project(":api"))

    ksp(project(":processor"))
    kspTest(project(":processor"))

    kspTest(project(":custompropertyinitializer"))
    testImplementation(project(":custompropertyinitializer"))

    testImplementation(platform("org.junit:junit-bom:5.10.2"))
    testImplementation("org.junit.jupiter:junit-jupiter")
    testImplementation("com.willowtreeapps.assertk:assertk:0.28.1")
}

tasks.jacocoTestReport {
    executionData(tasks.test.get())

    reports {
        xml.required.set(true)
        csv.required.set(false)
        html.required.set(true)
    }
}

tasks.withType(Test::class.java).configureEach {
    testLogging {
        events = setOf(TestLogEvent.PASSED, TestLogEvent.FAILED, TestLogEvent.SKIPPED)
    }

    useJUnitPlatform()
}



