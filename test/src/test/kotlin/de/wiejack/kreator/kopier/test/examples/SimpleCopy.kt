package de.wiejack.kreator.kopier.test.examples

import de.wiejack.kreator.builder.api.Builder
import de.wiejack.kreator.kopier.api.Copy

@Copy
@Builder
class SimpleCopy(val aString: String, val anInt: Int, val aBoolean: Boolean?) {
    var aDouble: Double? = null

    companion object
}
