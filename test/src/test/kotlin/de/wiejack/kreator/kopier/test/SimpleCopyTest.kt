package de.wiejack.kreator.kopier.test

import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isEqualToIgnoringGivenProperties
import assertk.assertions.isNotSameAs
import assertk.assertions.isNotSameInstanceAs
import de.wiejack.kreator.kopier.test.examples.SimpleCopy
import de.wiejack.kreator.kopier.test.examples.builder
import de.wiejack.kreator.kopier.test.examples.copy
import org.junit.jupiter.api.Test

class SimpleCopyTest {

    @Test
    fun `create a copy`() {
        val original = SimpleCopy.builder {  }

        val aCopy = original.copy { aString = "two" }

        assertThat(aCopy).isNotSameInstanceAs(original)
        assertThat(aCopy.aString).isEqualTo("two")
        assertThat(aCopy).isEqualToIgnoringGivenProperties(original, SimpleCopy::aString)
    }
}
