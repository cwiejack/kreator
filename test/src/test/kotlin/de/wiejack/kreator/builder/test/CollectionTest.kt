package de.wiejack.kreator.builder.test

import assertk.assertThat
import assertk.assertions.hasSize
import assertk.assertions.isEqualTo
import de.wiejack.kreator.builder.api.withRandomValues
import de.wiejack.kreator.builder.test.examples.CollectionExample
import de.wiejack.kreator.builder.test.examples.builder
import org.junit.jupiter.api.Test
import java.util.UUID

internal class CollectionTest {
    @Test
    fun `builder with collection dsl`() {
        val randomString = UUID.randomUUID().toString()
        val example =
            CollectionExample.builder {
                stringList {
                    +"a string"
                    this add "another string"
                }
                intSet {
                    this add 42
                }
                map {
                    this add ("key" to 42)
                    +(randomString to 43)
                    add("anotherKey" to 44)
                }
            }

        with(example) {
            assertThat(stringList).hasSize(2)
            assertThat(stringList.first()).isEqualTo("a string")
            assertThat(stringList.last()).isEqualTo("another string")

            assertThat(intSet).hasSize(1)
            assertThat(intSet.first()).isEqualTo(42)

            assertThat(map).hasSize(3)
            assertThat(map["key"]).isEqualTo(42)
            assertThat(map[randomString]).isEqualTo(43)
            assertThat(map["anotherKey"]).isEqualTo(44)
        }
    }

    @Test
    fun `use collection dsl magic`() {
        val example =
            CollectionExample.builder {
                stringList {
                    withRandomValues(4)
                }
                intSet {
                    withRandomValues(3)
                }
            }

        with(example) {
            assertThat(stringList).hasSize(4)
            assertThat(intSet).hasSize(3)
        }
    }
}
