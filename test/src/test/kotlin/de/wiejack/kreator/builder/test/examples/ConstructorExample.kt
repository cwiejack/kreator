package de.wiejack.kreator.builder.test.examples

import de.wiejack.kreator.builder.api.Builder
import de.wiejack.kreator.builder.api.BuilderConstructor
import de.wiejack.kreator.builder.api.NoBuilder

@Builder
class ConstructorExample(val one: Long) {

    lateinit var two: String

    @NoBuilder
    var usedSecondConstructor: Boolean? = null

    @BuilderConstructor
    constructor(one: Long, two: String) : this(one) {
        this.two = two
        usedSecondConstructor = true
    }

    companion object {}
}
