package de.wiejack.kreator.builder.test

import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isNull
import de.wiejack.kreator.builder.test.examples.PrefilledCollectionExample
import de.wiejack.kreator.builder.test.examples.builder
import org.junit.jupiter.api.Test

class PrefilledCollectionTest {
    @Test
    fun `builder creates instance with prefilled collections`() {
        val example = PrefilledCollectionExample.builder { }

        assertThat(example.uuidList.size).isEqualTo(3)
        assertThat(example.localDateSet.size).isEqualTo(3)
        assertThat(example.nullableList).isNull()

        assertThat(example.stringList.size).isEqualTo(4)
    }
}
