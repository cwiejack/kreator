package de.wiejack.kreator.builder.test

import assertk.assertThat
import assertk.assertions.isEqualTo
import de.wiejack.kreator.builder.test.examples.ConstructorExample
import de.wiejack.kreator.builder.test.examples.builder
import org.junit.jupiter.api.Test

internal class ConstructorTest {
    @Test
    fun `use annotated constructor`() {
        val example = ConstructorExample.builder {
            one = 42L
            two = "a string"
        }

        with(example) {
            assertThat(one).isEqualTo(42L)
            assertThat(two).isEqualTo("a string")
            assertThat(usedSecondConstructor).isEqualTo(true)
        }
    }
}
