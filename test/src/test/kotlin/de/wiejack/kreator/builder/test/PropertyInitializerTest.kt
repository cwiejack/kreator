package de.wiejack.kreator.builder.test

import assertk.assertThat
import assertk.assertions.isEqualTo
import de.wiejack.kreator.builder.test.examples.PropertyInitializerExample
import de.wiejack.kreator.builder.test.examples.builder
import org.junit.jupiter.api.Test

class PropertyInitializerTest {

    @Test
    fun `PropertyInitializer annotation overrides defaults`() {
        val example = PropertyInitializerExample.builder { }

        assertThat(example.aString).isEqualTo("The answer is 42.")
        assertThat(example.anInt).isEqualTo(42)
    }
}
