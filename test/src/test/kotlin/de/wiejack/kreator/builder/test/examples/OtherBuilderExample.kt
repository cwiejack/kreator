package de.wiejack.kreator.builder.test.examples

import de.wiejack.kreator.builder.api.Builder

@Builder
class OtherBuilderExample(
    val buildable: BuildableExample,
    val nullableBuildable: BuildableExample?
) {
    companion object {}
}

@Builder
class BuildableExample(val aString: String) {
    companion object {}
}
