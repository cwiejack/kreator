package de.wiejack.kreator.builder.test

import assertk.assertThat
import assertk.assertions.isNotNull
import assertk.assertions.isNull
import de.wiejack.kreator.builder.test.examples.NullableTypeExample
import de.wiejack.kreator.builder.test.examples.builder
import de.wiejack.kreator.builder.test.examples.initializeAllNullableProperties
import org.junit.jupiter.api.Test

class NullableTypeTest {

    @Test
    fun `initializeNullProperties set all nullable types with default values`() {

        val nullNotInitialized = NullableTypeExample.builder { }

        with(nullNotInitialized) {
            assertThat(aNullString).isNull()
            assertThat(aNullInt).isNull()
            assertThat(aNullDouble).isNull()
            assertThat(aNullFloat).isNull()
            assertThat(aNullLong).isNull()
            assertThat(aNullBoolean).isNull()
            assertThat(aNullUUID).isNull()
            assertThat(aNullList).isNull()
            assertThat(aNullSet).isNull()
            assertThat(aNullEnum).isNull()
            assertThat(someCustomClass).isNull()
        }

        val nullInitialized = NullableTypeExample.builder {
            this.initializeAllNullableProperties()
        }

        with(nullInitialized) {
            assertThat(aNullString).isNotNull()
            assertThat(aNullInt).isNotNull()
            assertThat(aNullDouble).isNotNull()
            assertThat(aNullFloat).isNotNull()
            assertThat(aNullLong).isNotNull()
            assertThat(aNullBoolean).isNotNull()
            assertThat(aNullUUID).isNotNull()
            assertThat(aNullList).isNotNull()
            assertThat(aNullSet).isNotNull()
            assertThat(aNullEnum).isNotNull()
            assertThat(someCustomClass).isNotNull()
        }
    }
}
