package de.wiejack.kreator.builder.test.examples

import de.wiejack.kreator.builder.api.Builder
import de.wiejack.kreator.builder.api.InitializeCollectionCount
import java.time.LocalDate
import java.util.UUID

@Builder(initializeCollectionsCount = 3)
class PrefilledCollectionExample(
    val uuidList: List<UUID>,
    val localDateSet: Set<LocalDate>,
    @InitializeCollectionCount(4)
    val stringList: List<String>,
    val nullableList: List<Boolean>?,
) {
    companion object
}
