package de.wiejack.kreator.builder.test

import assertk.assertThat
import assertk.assertions.isEqualTo
import de.wiejack.kreator.builder.test.examples.ParentBuilderExtensionExample
import de.wiejack.kreator.builder.test.examples.builder
import de.wiejack.kreator.builder.test.examples.changeBoth
import de.wiejack.kreator.builder.test.examples.changeInt
import de.wiejack.kreator.builder.test.examples.changeString
import org.junit.jupiter.api.Test

class BuilderExtensionExampleTest {

    @Test
    fun `use multiple extensions could be combined`() {
        val example = ParentBuilderExtensionExample.builder {
            builderExtensionExample {
                changeString("another string")
                changeInt(2)
            }
        }

        assertThat(example.builderExtensionExample.aString).isEqualTo("another string")
        assertThat(example.builderExtensionExample.anInt).isEqualTo(2)
    }

    @Test
    fun `use multiple extensions does not interfere`() {
        val example = ParentBuilderExtensionExample.builder {
            builderExtensionExample {
                changeBoth("another string", 2)
                changeInt(42)
            }
        }

        assertThat(example.builderExtensionExample.aString).isEqualTo("another string")
        assertThat(example.builderExtensionExample.anInt).isEqualTo(42)
    }

    @Test
    fun `extensions order is respected`() {
        val example = ParentBuilderExtensionExample.builder {
            builderExtensionExample {
                changeInt(42)
                changeBoth("another string", 2)
            }
        }

        assertThat(example.builderExtensionExample.aString).isEqualTo("another string")
        assertThat(example.builderExtensionExample.anInt).isEqualTo(2)
    }
}
