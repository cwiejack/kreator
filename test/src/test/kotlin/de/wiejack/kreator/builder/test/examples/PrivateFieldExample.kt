package de.wiejack.kreator.builder.test.examples

import de.wiejack.kreator.builder.api.Builder
import java.util.UUID

@Builder
class PrivateFieldExample {

    private var someUUID: UUID = UUID.randomUUID()

    companion object {}
}
