package de.wiejack.kreator.builder.test.examples

import de.wiejack.kreator.builder.api.Builder

@Builder
class ParentBuilderExtensionExample(
    val builderExtensionExample: BuilderExtensionExample,
) {
    companion object
}

@Builder
class BuilderExtensionExample(
    val aString: String,
    val anInt: Int,
) {
    companion object
}

fun BuilderExtensionExampleBuilder.changeString(newString: String) {
    aString = newString
}

fun BuilderExtensionExampleBuilder.changeInt(newInt: Int) {
    anInt = newInt
}

fun BuilderExtensionExampleBuilder.changeBoth(newString: String, newInt: Int) {
    aString = newString
    anInt = newInt
}
