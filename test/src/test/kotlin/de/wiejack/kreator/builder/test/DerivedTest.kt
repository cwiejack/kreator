package de.wiejack.kreator.builder.test

import de.wiejack.kreator.builder.test.examples.SealedDerivedExample
import de.wiejack.kreator.builder.test.examples.builder
import org.junit.jupiter.api.Test

internal class DerivedTest {

    @Test
    internal fun `sealed subtype used for building`() {
        SealedDerivedExample.builder { }
    }

}
