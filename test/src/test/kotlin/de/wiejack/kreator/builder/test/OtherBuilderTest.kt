package de.wiejack.kreator.builder.test

import assertk.assertThat
import assertk.assertions.isEqualTo
import de.wiejack.kreator.builder.test.examples.BuildableExampleBuilder
import de.wiejack.kreator.builder.test.examples.OtherBuilderExample
import de.wiejack.kreator.builder.test.examples.builder
import org.junit.jupiter.api.Test

internal class OtherBuilderTest {

    @Test
    fun `build multiple objects with builder`() {

        val example = OtherBuilderExample.builder {
            buildable  {
                aString = "a string"
            }
            nullableBuildable = {
                aString = "another string"
            }
        }

        assertThat(example.buildable.aString).isEqualTo("a string")
        assertThat(example.nullableBuildable?.aString).isEqualTo("another string")
    }
}
