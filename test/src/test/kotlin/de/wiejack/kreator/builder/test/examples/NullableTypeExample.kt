package de.wiejack.kreator.builder.test.examples

import de.wiejack.kreator.builder.api.Builder

@Builder
class SomeCustomClass(
    val aString: String,
) {
    companion object
}
@Builder
class NullableTypeExample {
    val aNullString: String? = null
    val aNullInt: Int? = null
    val aNullDouble: Double? = null
    val aNullFloat: Float? = null
    val aNullLong: Long? = null
    val aNullBoolean: Boolean? = null
    val aNullUUID: java.util.UUID? = null
    val aNullList: List<String>? = null
    val aNullSet: Set<String>? = null
    val aNullEnum: TestEnum? = null
    val someCustomClass: SomeCustomClass? = null

    companion object;

    enum class TestEnum {
        A
    }
}

@Builder(initializeNullableProperties = true)
class NullableTypeExampleWithInitializeNullablePropertiesTrue {
    val aNullString: String? = null
    val aNullInt: Int? = null
    val aNullDouble: Double? = null
    val aNullFloat: Float? = null
    val aNullLong: Long? = null
    val aNullBoolean: Boolean? = null
    val aNullUUID: java.util.UUID? = null
    val aNullList: List<String>? = null
    val aNullSet: Set<String>? = null
    val aNullEnum: TestEnum? = null
    val aNullLocalDate: java.time.LocalDate? = null
    val aNullOffsetDateTime: java.time.OffsetDateTime? = null
    val someCustomClass: SomeCustomClass? = null

    companion object;

    enum class TestEnum {
        A
    }
}
