package de.wiejack.kreator.builder.test.examples

import de.wiejack.kreator.builder.api.Builder

@Builder
class SealedDerivedExample(
    val movie: Movie,
) {
    companion object {}
}

sealed class Movie(
    val name: String,
) {
    @Builder
    class ActionMovie(
        name: String,
        val demolitions: Int,
    ) : Movie(name) {
        companion object
    }

    @Builder
    class RomanticMovie(
        name: String,
        val kisses: Int,
    ) : Movie(name) {
        companion object
    }
}
