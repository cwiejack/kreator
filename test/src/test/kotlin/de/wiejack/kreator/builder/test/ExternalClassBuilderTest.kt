package de.wiejack.kreator.builder.test

import assertk.assertThat
import assertk.assertions.isNotNull
import de.wiejack.kreator.builder.test.examples.ExternalClassExample
import de.wiejack.kreator.builder.test.examples.builder
import org.junit.jupiter.api.Test

class ExternalClassBuilderTest {

    @Test
    fun `external property initializer by serviceloader is used`() {
        val classWithCustomProperty = ExternalClassExample.builder { }
        assertThat(classWithCustomProperty.anExternalClass.aCustomString).isNotNull()
    }
}
