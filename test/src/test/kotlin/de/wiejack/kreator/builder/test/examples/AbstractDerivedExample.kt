package de.wiejack.kreator.builder.test.examples

import de.wiejack.kreator.builder.api.Builder

@Builder
class AbstractDerivedExample(
    val vehicle: Vehicle,
) {
    companion object {}
}
abstract class Vehicle(val name: String)

@Builder
class Bike(name: String, val price: Double) : Vehicle(name) {
    companion object
}

@Builder
class Boat(name: String, val color: String) : Vehicle(name) {
    companion object
}
