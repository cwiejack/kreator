package de.wiejack.kreator.builder.test.examples

import de.wiejack.kreator.builder.api.Builder
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*

@Builder
class SimpleTypeExample(
    val aString: String,
    val aNullableString: String?,
    val anInt: Int,
    val aNullableInt: Int?,
    val aDouble: Double,
    val aNullableDouble: Double?,
    val aLong: Long,
    val aNullableLong: Long?,
    val aNumber: Number,
    val aNullableNumber: Number?,
    val aShort: Short,
    val aNullableShort: Short?,
    val aFloat: Float,
    val aNullableFloat: Float?,
    val aBoolean: Boolean,
    val aNullableBoolean: Boolean?,
    val aUUID: UUID,
    val aNullableUUID: UUID?,
    val anInstant: Instant,
    val aNullableInstant: Instant?,
    val aLocalDate: LocalDate,
    val aNullableLocalDate: LocalDate?,
    val aLocalDateTime: LocalDateTime,
    val aNullableLocalDateTime: LocalDateTime?,
    val anEnum: SimpleEnum,
    val aNullableEnum: SimpleEnum?,
    val aLocale: Locale,
    val aNullableLocale: Locale?,
    val aTimeZone: TimeZone,
    val aNullableTimeZone: TimeZone?,
    val aCurrency: Currency,
    val aNullableCurrency: Currency?,
    val aChar: Char,
    val aNullableChar: Char?,
    val aByte: Byte,
    val aNullableByte: Byte?,
    val aByteArray: ByteArray,
    val aNullableByteArray: ByteArray?,
    val aLongArray: LongArray,
    val aNullableLongArray: LongArray?,
    val anIntArray: IntArray,
    val aNullableIntArray: IntArray?,
    val aFloatArray: FloatArray,
    val aNullableFloatArray: FloatArray?,
    val aDoubleArray: DoubleArray,
    val aNullableDoubleArray: DoubleArray?,
    val aShortArray: ShortArray,
    val aNullableShortArray: ShortArray?,
) {
    companion object {}
}

enum class SimpleEnum {
    A,
    B,
}
