package de.wiejack.kreator.builder.test.examples

import de.wiejack.kreator.builder.api.Builder

@Builder
class CollectionExample(
    val stringList: List<String>,
    val intSet: Set<Int>,
    val nullableList: List<Boolean>?,
    val map: Map<String, Int>,
) {
    companion object
}
