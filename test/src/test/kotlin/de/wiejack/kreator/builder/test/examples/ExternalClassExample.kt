package de.wiejack.kreator.builder.test.examples

import de.wiejack.custom.CustomClass
import de.wiejack.kreator.builder.api.Builder

@Builder
class ExternalClassExample {
    lateinit var anExternalClass: CustomClass

    companion object
}
