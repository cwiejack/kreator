package de.wiejack.kreator.builder.test.examples

import de.wiejack.kreator.builder.api.Builder

@Builder(useCompanionObject = false)
class RootFunctionInitializerExample(
    val aString: String,
    val aNullableString: String?,
)
