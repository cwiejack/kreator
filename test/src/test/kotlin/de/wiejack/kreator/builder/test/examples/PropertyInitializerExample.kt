package de.wiejack.kreator.builder.test.examples

import de.wiejack.kreator.builder.api.Builder
import de.wiejack.kreator.builder.api.PropertyInitializer
@Builder
class PropertyInitializerExample(
    @PropertyInitializer(initializerString = "The answer is 42.")
    val aString: String,
    @PropertyInitializer(initializerValue = "42")
    val anInt: Int,
    @PropertyInitializer(
        initializerValue = "PropertyInitializeImport.magicNumber()",
        imports = ["de.wiejack.kreator.builder.test.examples.PropertyInitializeImport"]
    )
    val magicNumber: Int

) {
    companion object
}

class PropertyInitializeImport {

    companion object {
        fun magicNumber() = 42
    }
}
