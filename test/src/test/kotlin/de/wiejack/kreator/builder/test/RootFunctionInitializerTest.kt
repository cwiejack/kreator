package de.wiejack.kreator.builder.test

import assertk.assertThat
import assertk.assertions.isNotNull
import assertk.assertions.isNull
import de.wiejack.kreator.builder.test.examples.RootFunctionInitializerExample
import de.wiejack.kreator.builder.test.examples.RootFunctionInitializerExampleBuilder
import de.wiejack.kreator.builder.test.examples.builder
import org.junit.jupiter.api.Test
import java.util.*

internal class RootFunctionInitializerTest {
    @Test
    internal fun `use root function instead of extension function for builder`() {
        val example: RootFunctionInitializerExample = RootFunctionInitializerExampleBuilder { }
        with(example) {
            assertThat(aString).isNotNull()
            assertThat(aNullableString).isNull()
        }
    }
}
