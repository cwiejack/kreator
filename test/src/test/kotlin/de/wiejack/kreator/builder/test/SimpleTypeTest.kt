package de.wiejack.kreator.builder.test

import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isNotNull
import assertk.assertions.isNull
import de.wiejack.kreator.builder.test.examples.SimpleEnum
import de.wiejack.kreator.builder.test.examples.SimpleTypeExample
import de.wiejack.kreator.builder.test.examples.builder
import org.junit.jupiter.api.Test
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*

internal class SimpleTypeTest {
    @Test
    internal fun `nullable properties get initialized with null`() {
        val example: SimpleTypeExample = SimpleTypeExample.builder { }
        with(example) {
            assertThat(aNullableString).isNull()
            assertThat(aNullableInt).isNull()
            assertThat(aNullableDouble).isNull()
            assertThat(aNullableLong).isNull()
            assertThat(aNullableNumber).isNull()
            assertThat(aNullableShort).isNull()
            assertThat(aNullableFloat).isNull()
            assertThat(aNullableBoolean).isNull()
            assertThat(aNullableUUID).isNull()
            assertThat(aNullableLocalDate).isNull()
            assertThat(aNullableLocalDateTime).isNull()
            assertThat(aNullableEnum).isNull()
            assertThat(aNullableLocale).isNull()
            assertThat(aNullableTimeZone).isNull()
            assertThat(aNullableCurrency).isNull()
            assertThat(aNullableChar).isNull()
            assertThat(aNullableFloatArray).isNull()
            assertThat(aNullableDoubleArray).isNull()
            assertThat(aNullableShortArray).isNull()
            assertThat(aNullableByteArray).isNull()
            assertThat(aNullableLongArray).isNull()
            assertThat(aNullableIntArray).isNull()
        }
    }

    @Test
    fun `nullable properties set to non null`() {
        val example: SimpleTypeExample =
            SimpleTypeExample.builder {
                aNullableString = "a"
                aNullableInt = 42
                aNullableDouble = 42.0
                aNullableLong = 42L
                aNullableNumber = 42
                aNullableShort = 42
                aNullableFloat = 42F
                aNullableBoolean = false
                aNullableUUID = UUID.randomUUID()
                aNullableLocalDate = LocalDate.now()
                aNullableLocalDateTime = LocalDateTime.now()
                aNullableEnum = SimpleEnum.A
                aNullableLocale = Locale.GERMANY
                aNullableCurrency = Currency.getInstance("EUR")
                aNullableTimeZone = TimeZone.getTimeZone("CET")
                aNullableChar = 'a'
                aNullableFloatArray = FloatArray(1)
                aNullableDoubleArray = DoubleArray(1)
                aNullableShortArray = ShortArray(1)
                aNullableByteArray = ByteArray(1)
                aNullableLongArray = LongArray(1)
                aNullableIntArray = IntArray(1)
            }

        with(example) {
            assertThat(aNullableString).isEqualTo("a")
            assertThat(aNullableInt).isEqualTo(42)
            assertThat(aNullableDouble).isEqualTo(42.0)
            assertThat(aNullableLong).isEqualTo(42L)
            assertThat(aNullableNumber).isEqualTo(42)
            assertThat(aNullableShort).isEqualTo(42)
            assertThat(aNullableFloat).isEqualTo(42F)
            assertThat(aNullableBoolean).isEqualTo(false)
            assertThat(aNullableUUID).isNotNull()
            assertThat(aNullableLocalDate).isNotNull()
            assertThat(aNullableLocalDateTime).isNotNull()
            assertThat(aNullableEnum).isEqualTo(SimpleEnum.A)
            assertThat(aNullableLocale).isEqualTo(Locale.GERMANY)
            assertThat(aNullableCurrency).isEqualTo(Currency.getInstance("EUR"))
            assertThat(aNullableTimeZone).isEqualTo(TimeZone.getTimeZone("CET"))
            assertThat(aNullableChar).isEqualTo('a')
            assertThat(aNullableFloatArray).isNotNull()
            assertThat(aNullableDoubleArray).isNotNull()
            assertThat(aNullableShortArray).isNotNull()
            assertThat(aNullableByteArray).isNotNull()
            assertThat(aNullableLongArray).isNotNull()
            assertThat(aNullableIntArray).isNotNull()
        }
    }
}
