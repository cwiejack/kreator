package de.wiejack.kreator.builder.test

import assertk.assertThat
import assertk.assertions.isEqualTo
import de.wiejack.kreator.findField
import de.wiejack.kreator.builder.test.examples.PrivateFieldExample
import de.wiejack.kreator.builder.test.examples.builder
import org.junit.jupiter.api.Test
import java.util.*

class PrivateFieldTest {

    @Test
    fun `could set a private field`() {
        val exampleUuid = UUID.randomUUID()
        val example = PrivateFieldExample.builder {
            someUUID = exampleUuid
        }

        val uuidField = findField(PrivateFieldExample::class.java, "someUUID")?.run {
            isAccessible = true
            get(example)
        }
        assertThat(uuidField).isEqualTo(exampleUuid)
    }
}
